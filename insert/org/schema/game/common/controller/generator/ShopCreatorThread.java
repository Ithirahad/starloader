//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.common.controller.generator;

import api.listener.events.controller.shop.ShopGenerateEvent;
import api.mod.StarLoader;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.data.world.Segment;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.controller.world.factory.WorldCreatorFactory;
import org.schema.game.server.controller.world.factory.WorldCreatorShopFactory;
import org.schema.game.server.controller.world.factory.WorldCreatorShopMannedFactory;

import java.util.Random;

public class ShopCreatorThread extends CreatorThread {
    private WorldCreatorFactory creator;

    public ShopCreatorThread(ShopSpaceStation var1) {
        super(var1);
        Random var2 = new Random(var1.getSeed());
        if (var1.getSeed() != 0L && var2.nextInt(32) != 0) {
            this.creator = new WorldCreatorShopFactory(var1.getSeed());
        } else {
            this.creator = new WorldCreatorShopMannedFactory(var1.getSeed());
        }
        //INSERTED CODE
        ShopGenerateEvent event = new ShopGenerateEvent(var1, this, creator);
        StarLoader.fireEvent(event, var1.isOnServer());
        this.creator = event.getFactory();
        ///
    }

    public int isConcurrent() {
        return 2;
    }

    public int loadFromDatabase(Segment var1) {
        return -1;
    }

    public void onNoExistingSegmentFound(Segment var1, RequestData var2) {
        this.creator.createWorld(this.getSegmentController(), var1, var2);
    }

    public boolean predictEmpty(Vector3i var1) {
        return false;
    }
}
