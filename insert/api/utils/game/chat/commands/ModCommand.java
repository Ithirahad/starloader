package api.utils.game.chat.commands;

import api.mod.ModSkeleton;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.game.PlayerUtils;
import api.utils.game.chat.CommandInterface;
import org.jetbrains.annotations.Nullable;
import org.schema.game.common.data.player.PlayerState;

/**
 * ModCommand
 * <Description>
 *
 * @author TheDerpGamer
 * @since 04/08/2021
 */
public class ModCommand implements CommandInterface {

    @Override
    public String getCommand() {
        return "mod";
    }

    @Override
    public String[] getAliases() {
        return new String[] {
                "mods",
                "modloader",
                "starloader"
        };
    }

    @Override
    public String getDescription() {
        return "Base command for managing mods.\n" +
                "- %COMMAND% list [search] : Lists all mods on server. Use the optional search argument to filter mods with similar or matching names.\n" +
                "- %COMMAND% list loaded [search] : Lists all loaded mods on server. Use the optional search argument to filter mods with similar or matching names.\n" +
                "- %COMMAND% list unloaded [search] : Lists all unloaded mods on server. Use the optional search argument to filter mods with similar or matching names.\n" +
                "- %COMMAND% load <name|all/*> : Loads the specified mod if it's not already loaded. Use \"all\" or \"*\" to load all unloaded mods.\n" +
                "- %COMMAND% unload <name|all/*> : Unloads the specified mod if it's currently loaded. Use \"all\" or \"*\" to unload all loaded mods.\n" +
                "- %COMMAND% reload <name|all/*> : Reloads the specified mod if it's currently loaded. Use \"all\" or \"*\" to reload all loaded mods.";
    }

    @Override
    public boolean isAdminOnly() {
        return true;
    }

    @Override
    public boolean onCommand(PlayerState sender, String[] args) {
        StringBuilder builder = new StringBuilder();
        if(args[0].equalsIgnoreCase("list")) {
            if(args.length == 1) { //%COMMAND% list
                builder.append("Mod List:\n");
                for(int i = 0; i < StarLoader.starMods.size(); i ++) {
                    ModSkeleton mod = StarLoader.starMods.get(i);
                    builder.append(mod.getName());
                    if(mod.isLoaded()) builder.append(" [LOADED]");
                    else builder.append(" [UNLOADED]");
                    if(i < StarLoader.starMods.size() - 1) builder.append("\n");
                }
                PlayerUtils.sendMessage(sender, builder.toString());
                return true;
            } else {
                if(args[1].equalsIgnoreCase("loaded")) {
                    if(args.length == 2) { //%COMMAND% list loaded
                        builder.append("Loaded Mods:\n");
                        for(int i = 0; i < StarLoader.starMods.size(); i ++) {
                            ModSkeleton mod = StarLoader.starMods.get(i);
                            if(mod.isLoaded()) builder.append(mod.getName());
                            if(i < StarLoader.starMods.size() - 1) builder.append("\n");
                        }
                    } else { //%COMMAND% list loaded [search...]
                        builder.append("Loaded Mods [\"").append(getArgsString(args, 2, args.length)).append("\"]:\n");
                        for(int i = 0; i < StarLoader.starMods.size(); i ++) {
                            ModSkeleton mod = StarLoader.starMods.get(i);
                            if(mod.isLoaded() && mod.getName().toLowerCase().contains(getArgsString(args, 2, args.length).toLowerCase())) {
                                builder.append(mod.getName());
                                if(i < StarLoader.starMods.size() - 1) builder.append("\n");
                            }
                        }
                    }
                    PlayerUtils.sendMessage(sender, builder.toString());
                    return true;
                } else if(args[1].equalsIgnoreCase("unloaded")) {
                    if(args.length == 2) { //%COMMAND% list unloaded
                        builder.append("Unloaded Mods:\n");
                        for(int i = 0; i < StarLoader.starMods.size(); i ++) {
                            ModSkeleton mod = StarLoader.starMods.get(i);
                            if(!mod.isLoaded()) builder.append(mod.getName());
                            if(i < StarLoader.starMods.size() - 1) builder.append("\n");
                        }
                    } else { //%COMMAND% list unloaded [search...]
                        builder.append("Unloaded Mods [\"").append(getArgsString(args, 2, args.length)).append("\"]:\n");
                        for(int i = 0; i < StarLoader.starMods.size(); i ++) {
                            ModSkeleton mod = StarLoader.starMods.get(i);
                            if(!mod.isLoaded() && mod.getName().toLowerCase().contains(getArgsString(args, 2, args.length).toLowerCase())) {
                                builder.append(mod.getName());
                                if(i < StarLoader.starMods.size() - 1) builder.append("\n");
                            }
                        }
                    }
                    PlayerUtils.sendMessage(sender, builder.toString());
                    return true;
                } else { //%COMMAND% list [search...]
                    builder.append("Mod List [\"").append(getArgsString(args, 2, args.length)).append("\"]:\n");
                    for(int i = 0; i < StarLoader.starMods.size(); i ++) {
                        ModSkeleton mod = StarLoader.starMods.get(i);
                        if(mod.getName().toLowerCase().contains(getArgsString(args, 2, args.length).toLowerCase())) {
                            builder.append(mod.getName());
                            if(i < StarLoader.starMods.size() - 1) builder.append("\n");
                        }
                    }
                    PlayerUtils.sendMessage(sender, builder.toString());
                    return true;
                }
            }
        } else if(args[0].equalsIgnoreCase("load")) {
            if(args.length == 2) {
                if(args[1].equalsIgnoreCase("all") || args[1].equalsIgnoreCase("*")) {
                    builder.append("Loaded the following mods:\n");
                    int j = 0;
                    for(int i = 0; i < StarLoader.starMods.size(); i ++) {
                        ModSkeleton modSkeleton = StarLoader.starMods.get(i);
                        if(!modSkeleton.isLoaded()) {
                            modSkeleton.setLoaded(true);
                            builder.append(modSkeleton.getName());
                            if(i < StarLoader.starMods.size() - 1) {
                                if(j < 8) {
                                    builder.append(", ");
                                    j ++;
                                } else {
                                    builder.append("\n");
                                    j = 0;
                                }
                            }
                        }
                    }
                } else {
                    ModSkeleton modSkeleton = StarLoader.getModFromName(args[1]);
                    if(modSkeleton != null) {
                        if(!modSkeleton.isLoaded()) {
                            modSkeleton.setLoaded(true);
                            builder.append("Loaded mod ").append(modSkeleton.getName());
                        } else builder.append("Mod ").append(modSkeleton.getName()).append(" was already loaded");
                    } else builder.append("Mod ").append(args[1]).append(" does not exist on server");
                }
            } else {
                ModSkeleton modSkeleton = StarLoader.getModFromName(getArgsString(args, 1, args.length));
                if(modSkeleton != null) {
                    if(!modSkeleton.isLoaded()) {
                        modSkeleton.setLoaded(true);
                        builder.append("Loaded mod ").append(getArgsString(args, 1, args.length));
                    } else builder.append("Mod ").append(getArgsString(args, 1, args.length)).append(" was already loaded");
                } else builder.append("Mod ").append(getArgsString(args, 1, args.length)).append(" does not exist on server");
            }
            PlayerUtils.sendMessage(sender, builder.toString());
            return true;
        } else if(args[0].equalsIgnoreCase("unload")) {
            if(args.length == 2) {
                if(args[1].equalsIgnoreCase("all") || args[1].equalsIgnoreCase("*")) {
                    builder.append("Unloaded the following mods:\n");
                    int j = 0;
                    for(int i = 0; i < StarLoader.starMods.size(); i ++) {
                        ModSkeleton modSkeleton = StarLoader.starMods.get(i);
                        if(modSkeleton.isLoaded()) {
                            modSkeleton.setLoaded(false);
                            builder.append(modSkeleton.getName());
                            if(i < StarLoader.starMods.size() - 1) {
                                if(j < 8) {
                                    builder.append(", ");
                                    j ++;
                                } else {
                                    builder.append("\n");
                                    j = 0;
                                }
                            }
                        }
                    }
                } else {
                    ModSkeleton modSkeleton = StarLoader.getModFromName(args[1]);
                    if(modSkeleton != null) {
                        if(modSkeleton.isLoaded()) {
                            modSkeleton.setLoaded(false);
                            builder.append("Unloaded mod ").append(modSkeleton.getName());
                        } else builder.append("Mod ").append(modSkeleton.getName()).append(" was already unloaded");
                    } else builder.append("Mod ").append(args[1]).append(" does not exist on server");
                }
            } else {
                ModSkeleton modSkeleton = StarLoader.getModFromName(getArgsString(args, 1, args.length));
                if(modSkeleton != null) {
                    if(modSkeleton.isLoaded()) {
                        modSkeleton.setLoaded(false);
                        builder.append("Unloaded mod ").append(getArgsString(args, 1, args.length));
                    } else builder.append("Mod ").append(getArgsString(args, 1, args.length)).append(" was already unloaded");
                } else builder.append("Mod ").append(getArgsString(args, 1, args.length)).append(" does not exist on server");
            }
            PlayerUtils.sendMessage(sender, builder.toString());
            return true;
        } else if(args[0].equalsIgnoreCase("reload")) {
            if(args.length == 2) {
                if(args[1].equalsIgnoreCase("all") || args[1].equalsIgnoreCase("*")) {
                    builder.append("Reloaded the following mods:\n");
                    int j = 0;
                    for(int i = 0; i < StarLoader.starMods.size(); i ++) {
                        ModSkeleton modSkeleton = StarLoader.starMods.get(i);
                        if(modSkeleton.isLoaded()) {
                            modSkeleton.setLoaded(false);
                            modSkeleton.setLoaded(true);
                            builder.append(modSkeleton.getName());
                            if(i < StarLoader.starMods.size() - 1) {
                                if(j < 8) {
                                    builder.append(", ");
                                    j ++;
                                } else {
                                    builder.append("\n");
                                    j = 0;
                                }
                            }
                        }
                    }
                } else {
                    ModSkeleton modSkeleton = StarLoader.getModFromName(args[1]);
                    if(modSkeleton != null) {
                        if(modSkeleton.isLoaded()) {
                            modSkeleton.setLoaded(false);
                            modSkeleton.setLoaded(true);
                            builder.append("Reloaded mod ").append(modSkeleton.getName());
                        } else builder.append("Mod ").append(modSkeleton.getName()).append(" wasn't loaded");
                    } else builder.append("Mod ").append(args[1]).append(" does not exist on server");
                }
            } else {
                ModSkeleton modSkeleton = StarLoader.getModFromName(getArgsString(args, 1, args.length));
                if(modSkeleton != null) {
                    if(modSkeleton.isLoaded()) {
                        modSkeleton.setLoaded(false);
                        modSkeleton.setLoaded(true);
                        builder.append("Reloaded mod ").append(getArgsString(args, 1, args.length));
                    } else builder.append("Mod ").append(getArgsString(args, 1, args.length)).append(" wasn't loaded");
                } else builder.append("Mod ").append(getArgsString(args, 1, args.length)).append(" does not exist on server");
            }
            PlayerUtils.sendMessage(sender, builder.toString());
            return true;
        }
        return false;
    }

    @Override
    public void serverAction(@Nullable PlayerState sender, String[] args) {

    }

    @Override
    public StarMod getMod() {
        return null;
    }

    private String getArgsString(String[] args, int start, int end) {
        StringBuilder builder = new StringBuilder();
        for(int i = start; i < end; i ++) {
            builder.append(args[i]);
            if(i < end - 1) builder.append(" ");
        }
        return builder.toString();
    }
}
