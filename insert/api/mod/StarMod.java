package api.mod;

import api.DebugFile;
import api.config.BlockConfig;
import api.listener.events.controller.ClientInitializeEvent;
import api.listener.events.controller.ServerInitializeEvent;
import api.mod.config.FileConfiguration;
import api.modloader.ProxyJarClassLoader;
import api.modloader.StarAgent;
import api.utils.particle.ModParticleUtil;
import org.apache.commons.io.IOUtils;
import org.schema.schine.resource.ResourceLoader;

import javax.validation.constraints.NotNull;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.instrument.ClassFileTransformer;
import java.security.ProtectionDomain;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class StarMod {
    ModSkeleton skeleton;

    public ModSkeleton getSkeleton() {
        return skeleton;
    }

    /**
     * Fired to core mods whenever a class is loaded.
     */
    public byte[] onClassTransform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] byteCode){
        return byteCode;
    }

    //===================== [ Life cycle events ] ==========================

    /**
     * Where mods are enabled before/during world load
     */
    public void onEnable(){
    }

    /**
     * When a player leaves a server, mods are disabled
     */
    public void onDisable(){

    }

    /**
     * Mods that do stuff directly when the game starts
     * Happens for EVERY mod, not just the ones enabled
     */
    public void onLoad(){

    }

    /**
     * is run when a serverstate instance is detected. Use it to run only serverside code.
     * added by IR0NSIGHT.
     */
    public void onServerCreated(ServerInitializeEvent event){
        DebugFile.log("Starmod was run with onServerCreated()",this);
    }

    /**
     * Method that is run when a GameClientState instance is made. Used to run only clientside code.
     * added by IR0NSIGHT.
     */
    public void onClientCreated(ClientInitializeEvent event){
        DebugFile.log("Starmod was run with onClientCreated()",this);
    }

    //
    private HashMap<String, FileConfiguration> config = new HashMap<String, FileConfiguration>();

    public FileConfiguration getConfig(String name){
        FileConfiguration namedConfig = config.get(name);
        if(namedConfig == null){
            FileConfiguration newConfig = new FileConfiguration(this, name);
            config.put(name, newConfig);
            return newConfig;
        }
        return namedConfig;
    }

    //

    public void addClassFileTransformer(ClassFileTransformer transformer){
        if(getSkeleton().isCoreMod()){
            StarAgent.instrumentation.addTransformer(transformer);
        }else{
            throw new IllegalStateException("Mod: " + this.getSkeleton().getDebugName() + " Tried to register a class transformer but was not a core mod.");
        }
    }

    // ========== Resource Loading Methods =============

    /**
     * Called when mods should register UniversalRegistry values (URVs)
     */
    public void onUniversalRegistryLoad() {

    }

    /**
     * Called after the BlockConfig is loaded.
     * @param config Dummy value for compatibility, BlockConfig is a static helper
     */
    public void onBlockConfigLoad(BlockConfig config){

    }

    public void onResourceLoad(ResourceLoader loader){

    }
    public void onLoadModParticles(ModParticleUtil.LoadEvent event){

    }


    public void setSkeleton(ModSkeleton skeleton) {
        this.skeleton = skeleton;
    }

    public String getName(){
        return getSkeleton().getName();
    }
    /**
     *
     * @param url The absolute path to the jar resource, like "me/jakev/extraeffects/res/cloud.png"
     * @return The InputStream of the resource, null if not found.
     */
    @NotNull
    public InputStream getJarResource(String url) throws IllegalArgumentException {
        InputStream resourceAsStream = getSkeleton().getClassLoader().getResourceAsStream(url);
        if(resourceAsStream == null) throw new IllegalArgumentException("Could not find resource: " + url + " | Mod: " + this.getSkeleton().getDebugName());
        return resourceAsStream;
    }

    //============= [ Class Ripper ] ================
    //You should probably just use ASM
    //Should look like: org.schema.common.whatever.ScanAddOn$3
    protected void forceDefine(String name){
        String classFileFromFQN = ProxyJarClassLoader.getClassFileFromFQN(name);
        try {
            ZipInputStream zip = new ZipInputStream(new FileInputStream(skeleton.getJarFile()));
            while (true) {
                ZipEntry entry = zip.getNextEntry();
                if (entry == null) break;
                System.err.println("Entry: " + entry.getName());
                System.err.println("FQN: " + classFileFromFQN);
                System.err.println("=-===============");
                if (entry.getName().equals(classFileFromFQN)) {
                    System.err.println("ENTRY NAME: " + classFileFromFQN + ", name=" + name);
                    ProxyJarClassLoader.hardDefineClass(name, IOUtils.toByteArray(zip));
                }
            }
            zip.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
