package api.utils.game.chat.commands;

import api.SMModLoader;
import api.mod.ModSkeleton;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.game.PlayerUtils;
import api.utils.game.chat.ChatCommand;
import org.schema.game.common.data.player.PlayerState;
import javax.annotation.Nullable;

/**
 * Created by Jake on 10/17/2020.
 * <insert description here>
 */
@Deprecated
public class SLBaseCommand extends ChatCommand {
    public SLBaseCommand(StarMod defaultMod) {
        super("starloader", "/starloader <load/unload/reload/mods> [mod]", "Mod commands", true, defaultMod);
    }

    @Override
    public void playOnServer(@Nullable PlayerState sender, String[] args) {
        if (args.length == 2) {
            switch (args[0]) {
                case "load":
                    PlayerUtils.sendMessage(sender, "Temporarily disabled.");
//                    try {
//                        StarMod starMod = SMModLoader.loadMod(new JarFile("mods/" + args[1] + ".jar"));
//                        starMod.onEnable();
//                        PlayerUtils.sendMessage(sender, "Mod loaded.");
//                    } catch (IOException | NullPointerException e) {
//                        PlayerUtils.sendMessage(sender, "Mod could not be loaded, check logs for details");
//                        e.printStackTrace();
//                    }
                    return;
                case "unload": {
                    ModSkeleton mod = StarLoader.getModFromName(args[1]);
                    if (mod == null) {
                        PlayerUtils.sendMessage(sender, "Mod not found");
                        return;
                    }
                    SMModLoader.unloadMod(mod);
                    PlayerUtils.sendMessage(sender, "Mod unloaded.");
                    return;
                }
                case "reload": {
                    ModSkeleton mod = StarLoader.getModFromName(args[1]);
                    if (mod == null) {
                        PlayerUtils.sendMessage(sender, "Mod not found");
                        return;
                    }
                    SMModLoader.unloadMod(mod);
                    StarMod starMod = null;
//                    try {
//                        starMod = SMModLoader.loadMod(new JarFile("mods/" + args[1] + ".jar"));
//                        starMod.onEnable();
//                        PlayerUtils.sendMessage(sender, "Mod reloaded.");
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                    PlayerUtils.sendMessage(sender, "loading temporarily disabled. remind me to fix it");
                    return;
                }
            }
        }else if(args.length == 1){
            if(args[0].equals("mods")){
                StringBuilder message = new StringBuilder("== Enabled Mods ==\n");
                for (ModSkeleton mod : StarLoader.starMods) {
                    message.append(mod.getDebugName()).append("\n");
                }
                PlayerUtils.sendMessage(sender, message.toString());
                return;
            }
        }else{
            PlayerUtils.sendMessage(sender, "Incorrect usage, use: " + getDescription());
        }
    }
}
