package api.mod;

import api.DebugFile;
import api.StarLoaderHooks;
import api.listener.Listener;
import api.listener.events.Event;
import api.listener.fastevents.FastListenerCommon;
import api.utils.game.chat.ChatCommand;
import api.utils.game.chat.CommandInterface;
import api.utils.other.HashList;
import javax.annotation.Nullable;
import java.util.*;


public class StarLoader{
    /**
     * Version of StarLoader
     * Versioning scheme: [Main].[Major feature].[Build]
     *
     * 0.x.xxx = Indev
     * 1.x.xxx = Main release
     * 2.x.xxx = Universe Update release
     */
    public static final String version = "0.4.041";
    public static final String versionName = "Alpha";
    public static ArrayList<ModSkeleton> starMods = new ArrayList<ModSkeleton>();
    public static HashMap<Class<? extends Event>, ArrayList<Listener>> listeners = new HashMap<Class<? extends Event>, ArrayList<Listener>>();
    @Deprecated
    private static HashMap<String, ChatCommand> commandsOld = new HashMap<>();

    private static HashList<StarMod, CommandInterface> commands = new HashList<>();

    public static void clearListeners() {
        listeners.clear();
        FastListenerCommon.clearAllListeners();
    }

    public static ArrayList<Listener> getListeners(Class<? extends Event> clazz) {
        return listeners.get(clazz);
    }

    public static void sortListeners(){
        for (Map.Entry<Class<? extends Event>, ArrayList<Listener>> entry : listeners.entrySet()) {
            ArrayList<Listener> entries = entry.getValue();
            Collections.sort(entries, new Comparator<Listener>() {
                @Override
                public int compare(Listener o1, Listener o2) {
                    return Integer.compare(o1.getPriority().ordinal(), o2.getPriority().ordinal());
                }
            });
        }
    }

    private static final ArrayList<ModSkeleton> coreMods = new ArrayList<>();
    public static void registerCoreMod(ModSkeleton mod) {
        coreMods.add(mod);
    }
    /**
     * Gets a list of all mods that need to use class transformers
     */
    public static ArrayList<ModSkeleton> getCoreMods(){
        return coreMods;
    }

    public static void enableMod(ModSkeleton mod) {
        DebugFile.log("== Enabling Mod " + mod.getName());
        if(!mod.getStarLoaderVersion().equals(StarLoader.version)){
            System.err.println("!! WARNING: Mod: " + mod.getDebugName() + " did not match StarLoader version [" + StarLoader.version + "], Mod SL Version: " + mod.getStarLoaderVersion());
        }
        StarLoaderHooks.onModEnableCommon(mod);
        mod.getRealMod().onEnable();
        mod.flagEnabled(true);
        DebugFile.log("== Mod " + mod.getName() + " Enabled");
    }

    public static void dumpModInfos(boolean enableOnly) {
        for (ModSkeleton mod : StarLoader.starMods) {
            if(!enableOnly || mod.isEnabled()) {
                DebugFile.log(mod.getDebugName());
            }
        }
    }
    public static <T extends Event> void registerListener(Class<T> clazz, StarMod mod, Listener<T> l) {
        registerListener(clazz, l, mod);
    }

    public static <T extends Event> void registerListener(Class<T> clazz, Listener<T> l, StarMod mod) {

        DebugFile.log("Registering listener " + clazz.getName());
        List<Listener> listeners = StarLoader.getListeners(clazz);
        l.setMod(mod);
        if (listeners == null) {
            ArrayList<Listener> new_listeners = new ArrayList<Listener>();
            new_listeners.add(l);
            StarLoader.listeners.put(clazz, new_listeners);
        } else {
            listeners.add(l);
        }
        sortListeners();
        DebugFile.log(" = Registered Listener. ");
    }

    /**
     * Passes an event of type clazz to all mods that have subscribed to it.
     */
    public static void fireEvent(Event ev, boolean isServer) {
        Event.fireEvent(ev, isServer);
    }

    //Legacy events, use fireEvent(Event, isServer);
    public static void fireEvent(Class<?> clazz, Event ev, boolean isServer) {
        Event.fireEvent(ev, isServer);
    }

    //private static ArrayList<ImmutablePair<String, String>> commands = new ArrayList<ImmutablePair<String, String>>();

    @Deprecated
    public static void registerCommand(ChatCommand command) {
        //commands.add(new ImmutablePair<String, String>(name, desc));
        commandsOld.put(command.getCommand(), command);
    }

    @Deprecated
    public static HashMap<String, ChatCommand> getCommands() {
        return commandsOld;
    }

    public static CommandInterface getCommand(String command) {
        for(CommandInterface cmd : getAllCommands()) {
            if(cmd.getCommand().equalsIgnoreCase(command)) return cmd; //Check if command is equal
            if(cmd.getAliases() != null && cmd.getAliases().length > 0) { //Check command aliases
                for(String alias : cmd.getAliases()) {
                    alias = alias.replace("%SEPARATOR%", " ");
                    if(alias.equalsIgnoreCase(command)) return cmd;
                }
            }
        }
        return null;
    }

    public static void registerCommand(CommandInterface command) {
        commands.add(command.getMod(), command);
    }

    public static ArrayList<CommandInterface> getModCommands(StarMod mod) {
        return commands.get(mod);
    }

    public static ArrayList<CommandInterface> getAllCommands() {
        ArrayList<CommandInterface> commandList = new ArrayList<>();
        for(ArrayList<CommandInterface> list : commands.values()) commandList.addAll(list);
        return commandList;
    }

    @Nullable
    public static <T> T getModFromMainClass(Class<T> modClass){
        for (ModSkeleton starMod : starMods) {
            if(starMod.getRealMod().getClass().equals(modClass)){
                return (T) starMod.getRealMod();
            }
        }
        return null;
    }

    //TODO Migrate to hashmap
    @Nullable
    public static ModSkeleton getModFromName(String name){
        for (ModSkeleton starMod : starMods) {
            if(starMod.getName().equals(name)){
                return starMod;
            }
        }
        return null;
    }
    @Nullable
    @Deprecated
    public static ModSkeleton getModFromId(int id){
        for (ModSkeleton starMod : starMods) {
            if(starMod.getSmdResourceId() == id){
                return starMod;
            }
        }
        return null;
    }

    @Nullable
    public static ModSkeleton getModFromId(ModIdentifier id){
        for (ModSkeleton starMod : starMods) {
            if(starMod.getSmdResourceId() == id.id && starMod.getModVersion().equals(id.version)){
                return starMod;
            }
        }
        return null;
    }

    public static String getVersionString() {
        return version + " " + versionName;
    }
}
