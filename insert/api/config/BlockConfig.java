package api.config;

import api.mod.StarMod;
import api.utils.registry.UniversalRegistry;
import api.utils.textures.StarLoaderTexture;
import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortArraySet;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.*;

import javax.annotation.Nullable;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class BlockConfig {
    public enum RailType {TRACK, ROTATOR, TURRET}
    public static void addCustomRail(ElementInformation info, RailType railType) {
        if(railType.equals(RailType.TRACK)) info.setBlockStyle(BlockStyle.NORMAL24.id);
        info.railType = railType;
    }

    /**
     * DO NOT USE THIS FOR MODDED ELEMENTS, config.add(info) will automatically do this for you
     * For new elements, this is not needed, however for vanilla items, they have already been inserted into the element hierarchy, so it needs to be re-added
     * @param info
     * @param category
     */
    public static void setElementCategory(ElementInformation info, ElementCategory category){
        ElementKeyMap.getCategoryHirarchy().removeRecursive(info);
        info.type = category;
        ElementKeyMap.getCategoryHirarchy().insertRecusrive(info);
    }

    /**
     * Creates a new item category. TOP
     */
    public static ElementCategory newElementCategory(ElementCategory top, String name){
        ElementCategory newCat = new ElementCategory(name, top);
        top.getChildren().add(newCat);
        return newCat;
    }

    public static void assignLod(ElementInformation info, StarMod mod, String lodName, @Nullable String activationLod){
        info.blended = true;
        info.individualSides = 6;
        info.blockStyle = BlockStyle.NORMAL24;
        info.physical = true;
        //Needs to be true for lod blocks for whatever reason
        info.drawOnlyInBuildMode = true;
        info.lodActivationAnimationStyle = 1;
        info.lodShapeStringActive = mod.getName() + "~" + lodName;
        info.lodShapeString = mod.getName() + "~" + lodName;
        info.placable = true;
        info.lodCollisionPhysical = true;
        info.cubeCubeCollision = true;
        info.blockResourceType = 2;
        info.blockStyle = BlockStyle.SPRITE;
    }

    /**
     *
     * @param factoryType [0=None, 1=Capsule Refinery, 2=Micro Assembler, 3=Basic Factory, 4=Standard Factory, 5=Advanced Factory, 6+ = Custom Factory]
     */
    public static void addRecipe(ElementInformation info, int factoryType, int bakeTime, FactoryResource... resources){
        info.consistence.clear();
        info.cubatomConsistence.clear();
        for (FactoryResource resource : resources){
            info.consistence.add(resource);
            info.cubatomConsistence.add(resource);
        }
        info.factoryBakeTime = bakeTime;
        info.producedInFactory = factoryType;
    }

    /**
     *
     * @param recipeArray The recipe array(FixedRecipe is a list of recipes) that you want to put the recipe in,
     *                   Example of a valid FixedRecipe: ElementKeyMap.capsuleRecipe
     */
    public static void addRefineryRecipe(FixedRecipe recipeArray, FactoryResource[] input, FactoryResource[] output){
        //my man schema really hates arraylists
        FixedRecipeProduct[] array = Arrays.copyOf(recipeArray.recipeProducts, recipeArray.recipeProducts.length + 1);
        FixedRecipeProduct product = new FixedRecipeProduct();
        product.input = input;
        product.output = output;
        array[array.length - 1] = product;
        recipeArray.recipeProducts = array;
    }
    public static void registerComputerModulePair(short computer, short module){
        ElementInformation comp = ElementKeyMap.infoArray[computer];
        comp.mainCombinationController = true;
        comp.systemBlock = true;
        comp.controlledBy.add((short) 1);
        comp.controlling.add(module);

        ElementKeyMap.infoArray[module].controlledBy.add(computer);
    }
    public static void setBlocksConnectable(ElementInformation master, ElementInformation slave){
//        master.setCanActivate(true);
//        slave.setCanActivate(true);
        master.controlling.add(slave.id);
        slave.controlledBy.add(master.id);
    }
    public static void clearRecipes(ElementInformation element){
        element.cubatomConsistence.clear();
        element.consistence.clear();
    }

    /**
     * Name/descriptions are controlled by these localization hashmaps.
     * You will need to update or remove them to set their name
     */
    public static void resetLocalization(ElementInformation info){
        ElementKeyMap.nameTranslations.remove(info.getId());
        ElementKeyMap.descriptionTranslations.remove(info.getId());
    }
    public static void setBasicInfo(ElementInformation info, String description, int price, float mass, boolean placeable, boolean activatable, int buildIcoNum){
        setBasicInfo(info, description, price, mass, placeable, activatable, buildIcoNum, 1, 64);
    }
    public static void setBasicInfo(ElementInformation info, String description, int price, float mass, boolean placeable, boolean activatable, int buildIcoNum, float volume, int blockHp){
        info.description = description;
        info.price = price;
        info.blockStyle = BlockStyle.NORMAL;
        info.setOrientatable(true);
        info.mass = mass;
        info.placable = placeable;
        info.volume = volume;
        info.maxHitPointsFull = blockHp;
        info.setCanActivate(activatable);
        info.setBuildIconNum(buildIcoNum);
    }
    private static final ShortArraySet factoryStandardControlledBy = new ShortArraySet();
    private static final ShortArraySet factoryStandardControlling = new ShortArraySet();
    static {
        //TODO add all of the other standards
        factoryStandardControlledBy.add(ElementKeyMap.STASH_ELEMENT);

        factoryStandardControlling.add(ElementKeyMap.CARGO_SPACE);
        factoryStandardControlling.add(ElementKeyMap.FACTORY_INPUT_ENH_ID);//Factory enhancer
    }

    public final static Short2IntOpenHashMap customFactories = new Short2IntOpenHashMap();
    private static int customFactoryIdLog = 20;
    private static int addedOres = 0;
    public static int getAddedOres() {
        return addedOres;
    }
    public static ElementInformation newFactory(StarMod mod, String name, short[] ids){
        ElementInformation elem = newElement(mod, name, ids);
        elem.buildIconNum = 231;
        elem.blockResourceType = 4;
        elem.basicResourceFactory = 0;
        elem.factory = new BlockFactory();
        elem.factory.enhancer = ElementKeyMap.FACTORY_INPUT_ENH_ID;
        elem.canActivate = true;
        elem.drawLogicConnection = true;

        elem.controlledBy.addAll(factoryStandardControlledBy);
        elem.controlling.addAll(factoryStandardControlling);
        for (Short id : elem.controlling) {
            ElementInformation e = ElementKeyMap.getInfo(id);
            e.controlledBy.add(elem.id);
        }

        factoryStandardControlledBy.add(elem.id);
        factoryStandardControlling.add(elem.id);

        customFactories.put(elem.id, customFactoryIdLog++);
        return elem;
    }
    public static ImmutablePair<ElementInformation, Integer> newOre(StarMod mod, String name, int buildIcon, BufferedImage image){
        addedOres++;
        ElementInformation block = newElement(mod, name, new short[]{186});

        //Ores are generally not placeable
        block.resourceInjection = ElementInformation.ResourceInjectionType.ORE;
        block.blockResourceType = 0;

        block.buildIconNum = buildIcon;
        block.inRecipe = true;
        block.individualSides = 1;
        block.blockStyle = BlockStyle.SPRITE;
        int resourceId = (int) UniversalRegistry.getExistingURV(UniversalRegistry.RegistryType.ORE, mod, name);

        //Bind a custom ore texture to the URV id.
        StarLoaderTexture starLoaderTexture = StarLoaderTexture.newOverlayTexture(image, resourceId);

        if(block.id > 2047){
            throw new RuntimeException("Ok so basically, the resIDToOrientationMapping array is too small. If you're seeing this ask me to convert it to a bimap");
        }
        if(resourceId > 255){
            throw new RuntimeException("You've allocated to many ores. If this ever happens, ask me to convert it to a bimap");
        }
        System.err.println("[StarLoader] Registering mod ore: " + resourceId + ", Name: " + block.getName());
        ElementKeyMap.resources[resourceId - 1] = block.id;
        ElementKeyMap.orientationToResIDMapping[resourceId] = block.id;
        ElementKeyMap.resIDToOrientationMapping[block.id] = (byte) resourceId;
        ElementKeyMap.orientationToResOverlayMapping[resourceId] = (byte) resourceId + 1;
        return new ImmutablePair<>(block, resourceId);
    }

    public static ElementInformation copyOfElement(StarMod mod, ElementInformation info, String newName){
        String namespacedName = mod.getName() + "~" + newName;
        short id = (short) ElementKeyMap.insertIntoProperties(namespacedName);
        return new ElementInformation(info, id, newName);
    }
    //Make sure that mods call .add after .newElement
    public static ArrayList<String> addedElementsDebug = new ArrayList<>();

    public static ElementInformation newElement(StarMod mod, String name, short... ids){
        String namespacedName = mod.getName() + "~" + name;
        addedElementsDebug.add(namespacedName);
        short id = (short) ElementKeyMap.insertIntoProperties(namespacedName);
        ElementInformation elementInformation = new ElementInformation(id, name, ElementKeyMap.getCategoryHirarchy(), ids);
        elementInformation.mod = mod;
        elementInformation.fullName = namespacedName;
        int idLength = ids.length;
        if(idLength == 3 || idLength == 6 || idLength == 1) {
            elementInformation.individualSides = idLength;
            if(idLength == 1){
                short t = ids[0];
                ids = new short[]{t,t,t,t,t,t};
                elementInformation.setTextureId(ids);
            }else if(idLength == 3){
                ids = new short[]{ids[0],ids[1],ids[0],ids[1],ids[0],ids[1]};
                elementInformation.setTextureId(ids);
            }else{
                elementInformation.setTextureId(ids);
            }
        } else {
            throw new IllegalArgumentException("You passed a texture array of: " + idLength + " to newElement, you must use sizes of 1, 3, or 6. ");
        }
        //todo resort at end
        ElementKeyMap.sortedByName.add(elementInformation);
        return elementInformation;
    }
    public static ElementInformation newChamber(StarMod mod, String name, short rootChamber, StatusEffectType appliedEffect){
        ElementInformation info = newElement(mod, name, new short[]{640});
        info.blockResourceType = 2;
        info.sourceReference = 1085;
        info.chamberRoot = rootChamber;
        info.chamberParent = 1085;
        info.chamberPermission = 1;
        info.chamberPrerequisites.add((short) 1085);
        info.placable = false;
        info.canActivate = true;
        info.systemBlock = true;

        info.price = 100;
        info.description = "A Custom chamber";
        info.shoppable = false;
        info.mass = 0.15F;

        info.chamberConfigGroupsLowerCase.add(appliedEffect.name().toLowerCase(Locale.ENGLISH));
        ElementKeyMap.chamberAnyTypes.add(info.getId());

        ElementInformation parentInfo = ElementKeyMap.getInfo(rootChamber);
        parentInfo.chamberChildren.add(info.getId());
        return info;
    }

    private static ArrayList<ElementInformation> elements = new ArrayList<ElementInformation>();
    public static ArrayList<ElementInformation> getElements(){
        return elements;
    }
    public static void add(ElementInformation entry) {
        //Add the elements to this list that doesnt really do anything
        elements.add(entry);
        addedElementsDebug.remove(entry.fullName);
        try {
                //Recreate the texture mappings so the game knows what textures to use
                Method m = ElementInformation.class.getDeclaredMethod("recreateTextureMapping");
                m.setAccessible(true);
                m.invoke(entry);
            //Add it
            ElementKeyMap.addInformationToExisting(entry);
        } catch (ParserConfigurationException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    public static void printElementDebug(){
        if(!addedElementsDebug.isEmpty()) {
            System.err.println("[WARNING] Some elements have been registered through newElement, but not added by .add!");
            for (String s : addedElementsDebug) {
                System.err.println("Element: " + s);
            }
            System.err.println("==========================================");
        }
    }
}
