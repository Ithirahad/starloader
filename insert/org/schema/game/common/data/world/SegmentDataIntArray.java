//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.common.data.world;

import api.DebugFile;
import api.utils.game.BlueprintModMappings;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.octree.ArrayOctree;
import org.schema.game.server.data.blueprint.SegmentControllerOutline;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public class SegmentDataIntArray extends SegmentData {
    private int[] data = new int[32768];
    boolean needsBitmapCompressionCheck;
    private static final ObjectArrayList<IntOpenHashSet> typeMapPool = new ObjectArrayList();
    private static final boolean USE_COMPRESSION_CHECK = true;

    public SegmentDataIntArray() {
    }

    public SegmentDataIntArray(boolean var1) {
        super(var1);
    }

    public SegmentDataIntArray(SegmentData var1) {
        super(var1);
        try {
            var1.checkWritable();
            System.arraycopy(var1.getAsIntBuffer(), 0, this.data, 0, 32768);
        } catch (SegmentDataWriteException var3) {
            var3.printStackTrace();
            throw new RuntimeException("needs to be implemented for other versions " + var1.getClass(), var3);
        }
    }

    public int[] getIntBuffer() throws SegmentDataWriteException {
        return this.data;
    }

    public void setType(int var1, short var2) throws SegmentDataWriteException {
        this.data[var1] = this.data[var1] & -2048 | var2;
    }

    public void setHitpointsByte(int var1, int var2) throws SegmentDataWriteException {
        assert var2 >= 0 && var2 < 128 : var2;

        short var3 = (short)Math.min(127, var2);
        this.data[var1] = this.data[var1] & -260097 | var3 << 11;
    }

    public void setActive(int var1, boolean var2) throws SegmentDataWriteException {
        this.data[var1] = var2 ? this.data[var1] | 262144 : this.data[var1] & -262145;

        assert var2 == this.isActive(var1) : var2 + "; " + this.isActive(var1);

    }

    public void setOrientation(int var1, byte var2) throws SegmentDataWriteException {
        assert var2 >= 0 && var2 < 32 : "NOT A SIDE INDEX " + var2;

        this.data[var1] = this.data[var1] & -16252929 | var2 << 19;

        assert var2 == this.getOrientation(var1) : "failed orientation coding: " + var2 + " != result " + this.getOrientation(var1);

    }

    public short getType(int var1) {
        return (short)(2047 & this.data[var1]);
    }

    public short getHitpointsByte(int var1) {
        return (short)((260096 & this.data[var1]) >> 11);
    }

    public boolean isActive(int var1) {
        return (262144 & this.data[var1]) > 0;
    }

    public void resetFast() throws SegmentDataWriteException {
        super.resetFast();
        Arrays.fill(this.data, 0);
    }

    public int[] getAsIntBuffer() {
        return this.data;
    }

    public boolean containsFast(int var1) {
        return (this.data[var1] & 2047) != 0;
    }

    public byte getOrientation(int var1) {
        return (byte)((16252928 & this.data[var1]) >> 19);
    }
    //INSERTED CODE
    public void translateModBlocks() throws SegmentDataWriteException {
        Segment segment = this.segment;
        if(segment == null){
            System.err.println("Outdated blueprint, not translating mod blocks");
            return;
        }
        String uid = segment.getSegmentController().getUniqueIdentifier();
        for (SegmentControllerOutline<?> outline : SegmentControllerOutline.shipUidsToConvert) {
            if(outline.uniqueIdentifier.equals(uid)){
                SegmentControllerOutline.shipUidsToConvert.remove(outline);
                assert outline.en instanceof BlueprintEntry : "Tried to check old blueprint";
                BlueprintEntry entry = (BlueprintEntry) outline.en;
                for (int i = 0; i < SegmentData.TOTAL_SIZE; i++) {
                    short oldId = getType(i);
                    if(oldId != 0) {
                        short newId = entry.getModMappings().translateId(oldId);
                        if (newId != oldId) {
                            System.err.println("TRANSLATE: " + oldId + " to " + newId);
                        }
                        setType(i, newId);
                    }
                }
                System.err.println("MM: " + entry.getModMappings());
                System.err.println("MMF: " + entry.getModMappingsFile());
                System.err.println("Name: " + entry.getName());
                entry.getModMappings().printState();
                DebugFile.info("> Current State: ");
                BlueprintModMappings.getCurrent().printState();
                break;
            }
        }

    }
    //
    public void deserialize(DataInput var1, long var2) throws IOException, SegmentDataWriteException {
        this.rwl.writeLock().lock();
        try {
            this.reset(var2);
            byte[] var6 = new byte[98304];//3*(32*32*32) 3bytes/block. 32 cube
            var1.readFully(var6);
            convertFrom3ByteToIntArray(var6, this.data);
            this.setNeedsRevalidate(true);
        } finally {
            this.rwl.writeLock().unlock();
        }

    }

    protected ArrayOctree getOctreeInstance(boolean var1) {
        return new ArrayOctree(var1);
    }

    public int applySegmentData(byte var1, byte var2, byte var3, int var4, int var5, boolean var6, long var7, boolean var9, boolean var10, long var11) throws SegmentDataWriteException {
        return this.applySegmentData(var1, var2, var3, var4, var5, var6, var7, var9, var10, var11, false);
    }

    public int applySegmentData(byte var1, byte var2, byte var3, int var4, int var5, boolean var6, long var7, boolean var9, boolean var10, long var11, boolean var13) throws SegmentDataWriteException {
        if (var6) {
            this.rwl.writeLock().lock();
        }

        try {
            int var14 = getInfoIndex(var1, var2, var3);
            boolean var15 = this.isActive(var14);
            short var16 = this.getType(var14);
            byte var17 = this.getOrientation(var14);
            this.data[var14 + var5] = var4;
            short var20 = this.getType(var14);
            short var21 = this.getHitpointsByte(var14);
            if (var20 == 0 && var16 == 0) {
                return 3;
            }

            if (var20 == var16) {
                if (this.getHitpointsByte(var14) != var21) {
                    return 2;
                }

                boolean var22 = this.isActive(var14);
                if (var15 != var22) {
                    if (!ElementKeyMap.isDoor(var20)) {
                        return 4;
                    }

                    if (var22 && !var15) {
                        assert var20 != 938;

                        this.octree.insert(var1, var2, var3, var14);
                        return 4;
                    }

                    if (!var22 && var15) {
                        this.octree.delete(var1, var2, var3, var14, var20);
                    }

                    return 4;
                }

                if (var17 != this.getOrientation(var14)) {
                    return 2;
                }

                return 3;
            }

            if (var16 == 0 && var20 != 0) {
                this.onAddingElement(var14, var1, var2, var3, var20, var10, var6, var7, var11);
                return 0;
            }

            if (var16 != 0 && var20 == 0) {
                this.onRemovingElement(var14, var1, var2, var3, var16, var9, var10, var17, var15, var6, var11, var13);
                return 1;
            }

            this.onRemovingElement(var14, var1, var2, var3, var16, var9, var10, var17, var15, var6, var11, var13);
            this.onAddingElement(var14, var1, var2, var3, var20, var10, var6, var7, var11);

            assert var20 != 0;
        } finally {
            if (var6) {
                this.rwl.writeLock().unlock();
            }

        }

        return 2;
    }

    public byte[] getAsByteBuffer(byte[] var1) {
        for(int var2 = 0; var2 < 32768; ++var2) {
            int var3 = var2 * 3;
            int var4 = this.data[var2];
            var1[var3] = (byte)var4;
            var1[var3 + 1] = (byte)(var4 >> 8);
            var1[var3 + 2] = (byte)(var4 >> 16);
        }

        return var1;
    }

    public void setInfoElementForcedAddUnsynched(int var1, int var2) throws SegmentDataWriteException {
        this.data[var1] = var2;
        this.setBlockAddedForced(true);
    }

    public int getDataAt(int var1) {
        return this.data[var1];
    }

    public int inflate(Inflater var1, byte[] var2) throws SegmentInflaterException, DataFormatException, SegmentDataWriteException {
        int var5;
        if ((var5 = var1.inflate(var2)) != this.data.length * 3) {
            throw new SegmentInflaterException(var5, this.data.length * 3);
        } else {
            int var3 = 0;

            for(int var4 = 0; var4 < 32768; ++var4) {
                this.data[var4] = convert3ByteToIntValue(var2[var3], var2[var3 + 1], var2[var3 + 2]);
                var3 += 3;
            }
            //INSERTED CODE
            try {
                translateModBlocks();
            } catch (SegmentDataWriteException e) {
                e.printStackTrace();
            }
            ///
            return var5;
        }
    }

    public void serialize(DataOutput var1) throws IOException {
        this.writeAs3Byte(var1, this.data);
    }

    public void getBytes(int var1, byte[] var2) {
        assert var2.length >= 3;

        var1 = this.data[var1];
        var2[0] = (byte)var1;
        var2[1] = (byte)(var1 >> 8);
        var2[2] = (byte)(var1 >> 16);
    }

    public void migrateTo(int var1, SegmentDataInterface var2) {
        if (var1 == 5) {
            System.arraycopy(this.data, 0, ((SegmentDataIntArray)var2).data, 0, this.data.length);
            ((SegmentDataIntArray)var2).needsBitmapCompressionCheck = true;
        } else if (var1 == 3) {
            System.arraycopy(this.data, 0, ((SegmentDataIntArray)var2).data, 0, this.data.length);
            ((SegmentDataIntArray)var2).needsBitmapCompressionCheck = true;
        } else if (var1 == 4) {
            System.arraycopy(this.data, 0, ((SegmentDataIntArray)var2).data, 0, this.data.length);
            ((SegmentDataIntArray)var2).repairAll();
            ((SegmentDataIntArray)var2).needsBitmapCompressionCheck = true;
        } else {
            assert false : var1;

        }
    }

    public void repairAll() {
        try {
            for(int var1 = 0; var1 < 32768; ++var1) {
                short var2;
                if (ElementKeyMap.isValidType(var2 = this.getType(var1))) {
                    this.getHitpointsByte(var1);
                    ElementKeyMap.getInfoFast(var2).getHpOldByte();
                    this.setHitpointsByte(var1, Math.min(127, 127));
                }
            }

        } catch (SegmentDataWriteException var3) {
            var3.printStackTrace();
        }
    }

    public SegmentDataType getDataType() {
        return SegmentDataType.INT_ARRAY;
    }

    public int[] getAsIntBuffer(int[] var1) {
        return this.data;
    }

    public static void freeStaticTypeMap(IntOpenHashSet var0) {
        var0.clear();
        synchronized(typeMapPool) {
            typeMapPool.add(var0);
        }
    }

    public static IntOpenHashSet getStaticTypeMap() {
        synchronized(typeMapPool) {
            return typeMapPool.isEmpty() ? new IntOpenHashSet(16) : (IntOpenHashSet)typeMapPool.remove(typeMapPool.size() - 1);
        }
    }

    public SegmentData doBitmapCompressionCheck(RemoteSegment var1) {
        if (this.needsBitmapCompressionCheck) {
            System.err.println("!!! BitMap compression check !!!");
            this.needsBitmapCompressionCheck = false;
            IntOpenHashSet var5 = getStaticTypeMap();

            for(int var2 = 0; var2 < 32768 && var5.size() <= 16; ++var2) {
                var5.add(this.data[var2]);
            }

            if (var5.size() <= 16) {
                int[] var7 = new int[var5.size()];
                int var3 = 0;

                for(Iterator var6 = var5.iterator(); var6.hasNext(); ++var3) {
                    int var4 = (Integer)var6.next();
                    var7[var3] = var4;
                }

                return new SegmentDataBitMap(!this.onServer, var7, this);
            }

            freeStaticTypeMap(var5);
        }

        return this;
    }

    public void setDataAt(int var1, int var2) throws SegmentDataWriteException {
        this.data[var1] = var2;
        this.setBlockAddedForced(true);
    }
}
