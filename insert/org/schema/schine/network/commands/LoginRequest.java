//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.schine.network.commands;

import api.StarLoaderHooks;
import api.listener.events.network.ClientLoginEvent;
import api.mod.StarLoader;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import org.schema.common.LogUtil;
import org.schema.schine.auth.SessionCallback;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.commands.Login.LoginCode;
import org.schema.schine.network.server.AuthenticationRequiredException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class LoginRequest implements Runnable {
    public static ObjectOpenHashSet<String> reserved;
    public String playerName;
    public String version;
    public String uid;
    public String lastLogin;
    public int id;
    public ServerProcessor serverProcessor;
    public short packetid;
    public Login login;
    public ServerStateInterface state;
    public String login_code;
    public byte userAgent;
    private int returnCode;
    private boolean authd = false;
    private RegisteredClientOnServer c;

    public LoginRequest() {
    }

    public static boolean isLoginNameValid(String var0) {
        if (var0.length() <= 0) {
            return false;
        } else if (var0.length() > 32) {
            return false;
        } else if (var0.length() <= 2) {
            return false;
        } else if (var0.matches("[_-]+")) {
            return false;
        } else if (reserved.contains(var0)) {
            return false;
        } else {
            return var0.matches("[a-zA-Z0-9_-]+");
        }
    }

    private void login() {
        System.err.println("[SERVER] PROCESSING LOGIN (" + this.playerName + ")");
        this.c = new RegisteredClientOnServer(this.id, this.playerName, this.state);
        this.c.setProcessor(this.serverProcessor);
        this.serverProcessor.setClient(this.c);
        SessionCallback var1 = this.state.getSessionCallBack(this.uid, this.login_code);

        try {
            if (!this.state.getController().authenticate(this.playerName, var1)) {
                throw new IllegalArgumentException();
            }

            this.authd = true;
        } catch (AuthenticationRequiredException var12) {
            var12.printStackTrace();
            this.returnCode = LoginCode.ERROR_AUTHENTICATION_FAILED_REQUIRED.code;
        } catch (IllegalArgumentException var13) {
            var13.printStackTrace();
            this.returnCode = LoginCode.ERROR_AUTHENTICATION_FAILED.code;
        }

        if (!isLoginNameValid(this.playerName)) {
            this.returnCode = LoginCode.ERROR_INVALID_USERNAME.code;
        }

        if (!this.state.checkUserAgent(this.userAgent, this.playerName)) {
            this.returnCode = LoginCode.ERROR_NOT_ADMIN.code;
        }

        synchronized(this.state) {
            this.state.setSynched();

            try {
                StringBuffer var3 = new StringBuffer();
                if (this.returnCode >= 0 && this.authd) {
                    System.err.println("[LOGIN] successful auth: now protecting username " + this.playerName + " with " + var1.getStarMadeUserName());
                    this.state.getController().protectUserName(this.playerName, var1);
                    this.c.setStarmadeName(var1.getStarMadeUserName());
                    this.c.setUpgradedAccount(var1.isUpgraded());
                    this.returnCode = this.state.getController().registerClient(this.c, this.version, var3);
                }

                String version = this.state.getVersion();
                if (this.returnCode < 0) {
                    System.err.println("[SERVER][LOGIN] login failed (" + LoginCode.getById(this.returnCode).name() + "): SET CLIENT TO NULL");
                    LogUtil.log().fine("[LOGIN] login failed for " + this.serverProcessor.getIp() + " (" + LoginCode.getById(this.returnCode).name() + ")");
                    this.serverProcessor.setClient((RegisteredClientOnServer)null);
                    this.login.createReturnToClient(this.state, this.serverProcessor, this.packetid, new Object[]{this.returnCode, this.id, System.currentTimeMillis(), version, var3.toString()});
                    this.serverProcessor.disconnectAfterSent();
                    if (!this.state.filterJoinMessages()) {
                        this.state.getController().broadcastMessage(new Object[]{482, this.playerName, LoginCode.getById(this.returnCode).name()}, 0);
                    }
                } else {
                    System.out.println("[SERVER][LOGIN] login received. returning login info for " + this.serverProcessor.getClient() + ": returnCode: " + this.returnCode);
                    //INSERTED CODE
                    ClientLoginEvent event = new ClientLoginEvent(this, this.returnCode, this.authd, version, serverProcessor, c, this.playerName);
                    StarLoaderHooks.onClientLoginEvent(event);
                    StarLoader.fireEvent(event, true);
                    ///
                    LogUtil.log().fine("[LOGIN] logged in " + this.serverProcessor.getClient() + " (" + this.serverProcessor.getIp() + ")");
                    this.login.createReturnToClient(this.state, this.serverProcessor, this.packetid, new Object[]{this.returnCode, this.id, System.currentTimeMillis(), version});
                    if (!this.state.filterJoinMessages()) {
                        this.state.getController().broadcastMessage(new Object[]{483, this.playerName}, 0);
                    }
                }
            } catch (Exception var9) {
                var9.printStackTrace();
                System.err.println("LOGIN EXCEPTION CATCH: " + var9.getClass() + " " + var9.getMessage());
            } finally {
                this.state.setUnsynched();
            }

        }
    }

    public void prepare() {
        System.err.println("[SERVER] PREPARING LOGIN (" + this.playerName + ")");
    }

    public void run() {
        this.login();
    }

    static {
        (reserved = new ObjectOpenHashSet()).add("[FACTION]");
        reserved.add("[STARMADE]");
        reserved.add("[SYSTEM]");
        reserved.add("[SERVER]");
    }
}
