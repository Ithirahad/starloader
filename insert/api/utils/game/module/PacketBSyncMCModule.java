package api.utils.game.module;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.network.objects.Sendable;

import java.io.IOException;

/**
 * Created by Jake on 12/17/2020.
 * <insert description here>
 */
public class PacketBSyncMCModule extends Packet {
    private ModManagerContainerModule module;

    public PacketBSyncMCModule(ModManagerContainerModule module) {
        this.module = module;
    }

    public PacketBSyncMCModule() {
    }

    @Override
    public void readPacketData(PacketReadBuffer buf) throws IOException {
        int blockId = buf.readInt();
        Sendable sendable = buf.readSendable();
        //todo move to respective correct locations
        if(sendable instanceof ManagedUsableSegmentController){
            ManagedUsableSegmentController<?> container = (ManagedUsableSegmentController<?>) sendable;
            ModManagerContainerModule module = container.getManagerContainer().getModMCModule((short) blockId);
            System.err.println("RECEIVED PACKET AND AM NOW DESERIALIZING");
            module.onTagDeserialize(buf);
        }else{
            throw new RuntimeException("");
        }
    }

    @Override
    public void writePacketData(PacketWriteBuffer buf) throws IOException {
        write(buf, module);
    }
    public static void write(PacketWriteBuffer buf, ModManagerContainerModule module) throws IOException {
        buf.writeInt(module.getBlockId());
        buf.writeSendable(module.getManagerContainer().getSegmentController());
        module.onTagSerialize(buf);
    }

    @Override
    public void processPacketOnClient() {

    }

    @Override
    public void processPacketOnServer(PlayerState sender) {

    }
}
