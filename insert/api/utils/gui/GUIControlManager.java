package api.utils.gui;

import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.Timer;

/**
 * GUIControlManager.java
 * GUI control manager class for mods to extend in their own GUI menus.
 *
 * @author TheDerpGamer
 * @since 03/18/2021
 */
public abstract class GUIControlManager extends AbstractControlManager {

    private boolean initialized;
    private GUIMenuPanel menuPanel;

    public GUIControlManager(GameClientState clientState) {
        super(clientState);
        initialized = false;
    }

    @Override
    public void setActive(boolean active) {
        if(active) {
            getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().suspend(true);
        } else {
            getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().suspend(false);
        }
        super.setActive(active);
    }

    @Override
    public void update(Timer timer) {
        CameraMouseState.setGrabbed(false);
        getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().suspend(true);
    }

    @Override
    public void onSwitch(boolean active) {
        if(active) {
            this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll large");
            this.setChanged();
            this.notifyObservers();
        } else {
            this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll small");
        }

        this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().suspend(active);
        this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().suspend(active);
    }

    public void onInit() {
        menuPanel = createMenuPanel();
        initialized = true;
    }

    public void draw() {
        if(!initialized) onInit();
        menuPanel.draw();
    }

    public void cleanUp() {
        if(initialized) menuPanel.cleanUp();
    }

    public abstract GUIMenuPanel createMenuPanel();
}