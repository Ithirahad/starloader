package org.schema.game.common.controller.damage.effects;

import api.listener.events.weapon.EffectDamageCalculateEvent;
import api.mod.StarLoader;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.HitReceiverType;
import org.schema.game.common.controller.damage.HitType;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;

public abstract class InterEffectHandler {
    private static final InterEffectHandler[] EFFECTS = new InterEffectHandler[InterEffectHandler.InterEffectType.values().length];

    public InterEffectHandler() {
    }

    public abstract InterEffectHandler.InterEffectType getType();

    public static float handleEffects(float var0, InterEffectSet var1, InterEffectSet var2, HitType var3, DamageDealerType var4, HitReceiverType var5, short var6) {
        float var7 = 0.0F;

        for(int var8 = 0; var8 < EFFECTS.length; ++var8) {
            var7 += EFFECTS[var8].getOutputDamage(var0, EFFECTS[var8].getType(), var1, var2, var3, var4, var5, var6);
        }
        //Maybe have another event here?
        return Math.max(0.0F, var7 / (float)EFFECTS.length);
    }

    public float getOutputDamage(float var1, InterEffectHandler.InterEffectType var2, InterEffectSet var3, InterEffectSet var4, HitType var5, DamageDealerType var6, HitReceiverType var7, short var8) {
        assert var3 != null;

        EffectDamageCalculateEvent dce = new EffectDamageCalculateEvent(
                var1,
                var1 * Math.max(0.0F, (var3 != null ? var3.getStrength(var2) : 0.0F) - (var4 != null ? var4.getStrength(var2) : 0.0F)), //Default formula (btw it's bad, pls fix)
                var2,
                var3,
                var4,
                var5,
                var6,
                var7,
                var8
        );
        StarLoader.fireEvent(dce, false);

        return dce.getResultDamage();
    }

    static {
        for(int var0 = 0; var0 < InterEffectHandler.InterEffectType.values().length; ++var0) {
            switch(InterEffectHandler.InterEffectType.values()[var0]) {
                case HEAT:
                    EFFECTS[var0] = new HeatEffectHandler();
                    break;
                case KIN:
                    EFFECTS[var0] = new KineticEffectHandler();
                    break;
                case EM:
                    EFFECTS[var0] = new EMEffectHandler();
                    break;
                default:
                    assert false;
            }

            assert EFFECTS[var0].getType() == InterEffectHandler.InterEffectType.values()[var0] : InterEffectHandler.InterEffectType.values()[var0] + "; " + var0;
        }

    }

    public static enum InterEffectType {
        HEAT("Heat", new Translatable() {
            public final String getName(Enum var1) {
                return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_EFFECTS_INTEREFFECTHANDLER_1;
            }
        }, new Translatable() {
            public final String getName(Enum var1) {
                return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_EFFECTS_INTEREFFECTHANDLER_0;
            }
        }),
        KIN("Kinetic", new Translatable() {
            public final String getName(Enum var1) {
                return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_EFFECTS_INTEREFFECTHANDLER_2;
            }
        }, new Translatable() {
            public final String getName(Enum var1) {
                return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_EFFECTS_INTEREFFECTHANDLER_3;
            }
        }),
        EM("EM", new Translatable() {
            public final String getName(Enum var1) {
                return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_EFFECTS_INTEREFFECTHANDLER_4;
            }
        }, new Translatable() {
            public final String getName(Enum var1) {
                return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_EFFECTS_INTEREFFECTHANDLER_5;
            }
        });

        public final Translatable fullName;
        public final Translatable shortName;
        public final String id;

        private InterEffectType(String var3, Translatable var4, Translatable var5) {
            this.id = var3;
            this.fullName = var5;
            this.shortName = var4;
        }

        public final String toString() {
            return this.id;
        }
    }
}
