package org.schema.game.client.view.mainmenu.gui.catalogmanager;

import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.game.server.data.blueprintnw.BlueprintType;
import org.schema.schine.graphicsengine.forms.gui.*;
import org.schema.schine.graphicsengine.forms.gui.newgui.*;
import org.schema.schine.input.InputState;
import java.util.*;

public class BlueprintEntryScrollableTableList extends ScrollableTableList<BlueprintEntry> implements GUIActiveInterface {

    private List<BlueprintEntry> blueprints;
    public boolean updated;

    public BlueprintEntryScrollableTableList(InputState state, GUIElement element) {
        super(state, 750, 500, element);
        updateBlueprints();
    }

    @Override
    public void initColumns() {
        new StringComparator();

        this.addColumn("Name", 18.0F, new Comparator<BlueprintEntry>() {
            public int compare(BlueprintEntry o1, BlueprintEntry o2) {
                return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
            }
        });

        this.addColumn("Mass", 7.5F, new Comparator<BlueprintEntry>() {
            public int compare(BlueprintEntry o1, BlueprintEntry o2) {
                return CompareTools.compare(o1.getMass(), o2.getMass());
            }
        });

        this.addColumn("Price", 7.5F, new Comparator<BlueprintEntry>() {
            public int compare(BlueprintEntry o1, BlueprintEntry o2) {
                return CompareTools.compare(o1.getPrice(), o2.getPrice());
            }
        });

        this.addColumn("Type", 5.0F, new Comparator<BlueprintEntry>() {
            public int compare(BlueprintEntry o1, BlueprintEntry o2) {
                return o1.getType().name().toLowerCase().compareTo(o2.getType().name().toLowerCase());
            }
        });

        this.addTextFilter(new GUIListFilterText<BlueprintEntry>() {
            public boolean isOk(String s, BlueprintEntry blueprint) {
                return blueprint.getName().toLowerCase().contains(s.toLowerCase());
            }
        }, ControllerElement.FilterRowStyle.LEFT);

        this.addDropdownFilter(new GUIListFilterDropdown<BlueprintEntry, BlueprintTypeFilter>(BlueprintTypeFilter.values()) {
            public boolean isOk(BlueprintTypeFilter type, BlueprintEntry blueprint) {
                switch(type) {
                    case ALL:
                        return true;
                    case SHIP:
                        return blueprint.getType().equals(BlueprintType.SHIP);
                    case STATION:
                        return blueprint.getType().equals(BlueprintType.SPACE_STATION);
                }
                return true;
            }

        }, new CreateGUIElementInterface<BlueprintTypeFilter>() {
            @Override
            public GUIElement create(BlueprintTypeFilter blueprintType) {
                GUIAncor anchor = new GUIAncor(getState(), 10.0F, 24.0F);
                GUITextOverlayTableDropDown dropDown;
                (dropDown = new GUITextOverlayTableDropDown(10, 10, getState())).setTextSimple(blueprintType.name());
                dropDown.setPos(4.0F, 4.0F, 0.0F);
                anchor.setUserPointer(blueprintType);
                anchor.attach(dropDown);
                return anchor;
            }

            @Override
            public GUIElement createNeutral() {
                return null;
            }
        }, ControllerElement.FilterRowStyle.RIGHT);

        this.activeSortColumnIndex = 0;
    }

    @Override
    protected Collection<BlueprintEntry> getElementList() {
        if(!updated) updateBlueprints();
        return blueprints;
    }

    public void updateBlueprints() {
        ElementKeyMap.initializeData(GameResourceLoader.getConfigInputFile());
        blueprints = BluePrintController.active.readBluePrints();
        flagDirty();
        updated = true;
    }

    @Override
    public void updateListEntries(GUIElementList list, Set<BlueprintEntry> set) {
        if(!updated) updateBlueprints();
        for(final BlueprintEntry blueprint : set) {

            GUITextOverlayTable nameTextElement = new GUITextOverlayTable(10, 10, this.getState());
            nameTextElement.setTextSimple(blueprint.getName());
            nameTextElement.setLimitTextDraw(30);
            GUIClippedRow nameRowElement;
            (nameRowElement = new GUIClippedRow(this.getState())).attach(nameTextElement);

            GUITextOverlayTable massTextElement;
            (massTextElement = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(StringTools.formatPointZero(blueprint.getMass()));
            GUIClippedRow massRowElement;
            (massRowElement = new GUIClippedRow(this.getState())).attach(massTextElement);

            GUITextOverlayTable priceTextElement;
            (priceTextElement = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(StringTools.formatPointZero(blueprint.getPrice()));
            GUIClippedRow priceRowElement;
            (priceRowElement = new GUIClippedRow(this.getState())).attach(priceTextElement);

            GUITextOverlayTable typeTextElement;
            (typeTextElement = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(blueprint.getType().name());
            GUIClippedRow typeRowElement;
            (typeRowElement = new GUIClippedRow(this.getState())).attach(typeTextElement);

            BlueprintEntryListRow blueprintListRow;
            (blueprintListRow = new BlueprintEntryListRow(this.getState(), blueprint, nameRowElement, massRowElement, priceRowElement, typeRowElement)).onInit();

            list.addWithoutUpdate(blueprintListRow);
        }

        list.updateDim();
    }

    public class BlueprintEntryListRow extends ScrollableTableList<BlueprintEntry>.Row {
        public BlueprintEntryListRow(InputState inputState, BlueprintEntry blueprint, GUIElement... guiElements) {
            super(inputState, blueprint, guiElements);
            this.highlightSelect = true;
            this.highlightSelectSimple = true;
            this.setAllwaysOneSelected(true);
        }
    }

    public enum BlueprintTypeFilter {
        ALL,
        SHIP,
        STATION
    }
}
