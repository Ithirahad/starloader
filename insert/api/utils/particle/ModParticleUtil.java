package api.utils.particle;

import api.DebugFile;
import api.ModPlayground;
import api.common.GameClient;
import api.common.GameServer;
import api.mod.StarMod;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.network.packets.PacketUtil;
import api.utils.textures.StarLoaderTexture;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.psys.modules.RendererModule;

import javax.imageio.ImageIO;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Jake on 12/4/2020.
 * <insert description here>
 */
public class ModParticleUtil {
    private static final ArrayList<BufferedImage> modSprites = new ArrayList<>();
    static final ArrayList<IModParticleFactory> factories = new ArrayList<>();

    static int registerParticleSprite(BufferedImage image) {
        modSprites.add(image);
        return modSprites.size() - 1;
    }

    static int registerParticle(IModParticleFactory factory) {
        factories.add(factory);
        return factories.size() - 1;
    }

    static ArrayList<Vector2f[]> pointMap = new ArrayList<>();
    static Sprite mainSprite;

    static void postRegisterParticles() {
        DebugFile.log("[StarLoader] Post register particles...");
        if (!modSprites.isEmpty()) {
            pointMap.clear();
            DebugFile.log("[StarLoader] Merging particle textures...");
//            BufferedImage mainTexture = null;
//            try {
//                mainTexture = ImageIO.read(new File("test.png"));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            final BufferedImage mainTexture = TextureSheetMergeAlgorithm.mergeTextures(modSprites, pointMap);

            try {
                File debugFile = new File("moddata/StarLoader/debug/MainParticleTexture.png");
                boolean mkdir = debugFile.getParentFile().mkdir();
                ImageIO.write(mainTexture, "png", debugFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

            modSprites.clear();
            DebugFile.log("[StarLoader] Creating sprite on graphics thread");
            StarLoaderTexture.runOnGraphicsThread(new Runnable() {
                @Override
                public void run() {
                    synchronized (ModParticleUtil.class) {
                        mainSprite = StarLoaderTexture.newSprite(mainTexture, ModPlayground.inst, "starloader_mainparticletexture", true, true);
                        mainSprite.setPositionCenter(false);
                    }
                }
            });
        } else {
            DebugFile.log("[StarLoader] Nothing to do");
        }
    }

    private static final ModParticleVertexBuffer vertexBuffer = new ModParticleVertexBuffer();

    //TODO Better data structure for particles, ArrayDeque but sortable probably
    private static final ArrayList<ModParticle> particles = new ArrayList<>();
    private static final ConcurrentLinkedQueue<ModParticle> particleAddQueue = new ConcurrentLinkedQueue<>();

    /**
     * Gets a random vector to a point on a sphere of radius rad
     */
    public static Vector3f getRandomDir(float rad) {
        Vector3f vector3f = new Vector3f(TextureSheetMergeAlgorithm.nextGaussian(),
                TextureSheetMergeAlgorithm.nextGaussian(),
                TextureSheetMergeAlgorithm.nextGaussian());
        vector3f.normalize();
        vector3f.scale(rad);
        return vector3f;
    }

    public static void playServer(List<PlayerState> players, int factoryId, Vector3f loc, int sprite, Builder builder) {
        if (GameServer.getServerState() == null) {
            return;
        }

        PacketSCPlayModParticles packet = new PacketSCPlayModParticles(factoryId, sprite, loc, builder);
        for (PlayerState player : players) {
            PacketUtil.sendPacket(player, packet);
        }
    }

    public static void playServer(Vector3i sector, int factoryId, Vector3f loc, int sprite, Builder builder) {
        if (GameServer.getServerState() == null) {
            return;
        }

        PacketSCPlayModParticles packet = new PacketSCPlayModParticles(factoryId, sprite, loc, builder);

        for (PlayerState player : getPlayersInRange(sector)) {
            PacketUtil.sendPacket(player, packet);
        }
    }

    private static ArrayList<PlayerState> getPlayersInRange(Vector3i sec) {
        ArrayList<PlayerState> r = new ArrayList<>();
        for (PlayerState value : GameServer.getServerState().getPlayerStatesByName().values()) {
            Vector3i currentSector = new Vector3i(value.getCurrentSector());
            currentSector.sub(sec);
            if (currentSector.lengthSquared() <= 4) {
                r.add(value);
            }
        }
        return r;
    }

    private static final Vector3f zeroVector = new Vector3f();
    public static void playClientDirect(ModParticle particle){
        particleAddQueue.add(particle);
    }
    public static void playClient(int factoryId, Vector3f loc, int sprite, Builder builder) {
        if (GameClient.getClientState() == null) {
            return;
        }
        //Unpack builder
        Vector3f v = builder.offset;
        float offsetX = builder.offset.x;
        float offsetY = builder.offset.y;
        float offsetZ = builder.offset.z;
        float amount = builder.getAmount();
        int lifetime = builder.getLifetime();
        ///
        IModParticleFactory factory = factories.get(factoryId);
        long ms = System.currentTimeMillis();
        for (int i = 0; i < amount; i++) {
            ModParticle particle;
            particle = factory.newParticle(factoryId, sprite, loc, builder);
            particle.lifetimeMs = lifetime;
            //Add random life to particle
            if (builder.randomLife != 0) {
                particle.lifetimeMs += TextureSheetMergeAlgorithm.randInt(builder.randomLife);
            }
            particle.startTime = ms;
            //Set position
            particle.position.set(loc);
            //Add offset to particles
            particle.position.x += TextureSheetMergeAlgorithm.randFloat(offsetX);
            particle.position.y += TextureSheetMergeAlgorithm.randFloat(offsetY);
            particle.position.z += TextureSheetMergeAlgorithm.randFloat(offsetZ);

            particle.velocity.set(builder.velocity);
            //If its emission burst, add `speed` as a random velocity
            if (builder.isEmissionBurst()) {
                particle.velocity.add(getRandomDir(ModPlayground.randFloat(0, builder.speed)));
            }
            //Update size
            particle.sizeX = builder.size.x;
            particle.sizeY = builder.size.y;

            //Update color
            particle.colorR = (byte) (builder.color.x*127);
            particle.colorG = (byte) (builder.color.y*127);
            particle.colorB = (byte) (builder.color.z*127);
            particle.colorA = (byte) (builder.color.w*127);

            particle.updateCameraDistance();
            particle.spawn();
            particle.particleSpriteId = sprite;
            particleAddQueue.add(particle);
        }
    }

    public static void drawAll() {
        long time = System.currentTimeMillis();

        particles.addAll(particleAddQueue);
        particleAddQueue.clear();

        if (particles.isEmpty()) {
            return;
        }

        beginDraw();

        Iterator<ModParticle> iterator = particles.iterator();

        while (iterator.hasNext()) {
            ModParticle particle = iterator.next();
            particle.update(time);
            particle.position.add(particle.velocity);
            particle.ticksLived++;
            particle.updateCameraDistance();
            if (time > particle.startTime + particle.lifetimeMs) {
                particle.die();
                iterator.remove();
            }
        }


        Collections.sort(particles);
        vertexBuffer.draw(particles, RendererModule.FrustumCullingMethod.NONE);
        endDraw();
    }

    private static void beginDraw() {
        GlUtil.glDisable(GL11.GL_LIGHTING);
        GlUtil.glEnable(GL11.GL_TEXTURE_2D);
        GlUtil.glEnable(GL11.GL_BLEND);
        GlUtil.glEnable(GL11.GL_DEPTH_TEST);
        GlUtil.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GlUtil.glBlendFuncSeparate(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GlUtil.glBindTexture(GL11.GL_TEXTURE_2D, mainSprite.getMaterial().getTexture().getTextureId());
        GlUtil.glEnable(GL11.GL_COLOR_MATERIAL);
    }

    private static void endDraw() {
        GlUtil.glBindTexture(GL11.GL_TEXTURE_2D, 0);
        GlUtil.glDisable(GL11.GL_BLEND);
        GlUtil.glDisable(GL11.GL_TEXTURE_2D);
        GlUtil.glDisable(GL11.GL_DEPTH_TEST);////////////// added this later
        GlUtil.glDisable(GL11.GL_COLOR_MATERIAL);
        GlUtil.glEnable(GL11.GL_LIGHTING);
    }

    public static class LoadEvent {
        public int addParticleSprite(BufferedImage image, StarMod mod) {
            return registerParticleSprite(image);
        }

        public int addParticle(IModParticleFactory factory, StarMod mod) {
            return registerParticle(factory);
        }
    }

    public static class Builder {
        private int amount = 1;
        private int lifetime = 1000;
        private float speed = 0;
        private int randomLife;
        private boolean emissionBurst = false;
        private Vector3f offset = new Vector3f(0, 0, 0);
        private Vector4f color = new Vector4f();
        private Vector3f velocity = new Vector3f();
        private Vector2f size = new Vector2f();

        public int getRandomLife() {
            return randomLife;
        }

        public Builder setRandomLife(int randomLife) {
            this.randomLife = randomLife;
            return this;
        }

        public Builder setAmount(int amount) {
            this.amount = amount;
            return this;
        }

        public Builder setLifetime(int lifetime) {
            this.lifetime = lifetime;
            return this;
        }

        public Builder setSpeed(float speed) {
            this.speed = speed;
            return this;
        }

        public Builder setEmissionBurst(boolean emissionBurst) {
            this.emissionBurst = emissionBurst;
            return this;
        }

        public Builder setOffset(Vector3f offset) {
            this.offset = offset;
            return this;
        }

        public Builder setOffset(float x, float y, float z) {
            this.offset.set(x, y, z);
            return this;
        }

        public int getAmount() {
            return amount;
        }

        public int getLifetime() {
            return lifetime;
        }

        public float getSpeed() {
            return speed;
        }

        public boolean isEmissionBurst() {
            return emissionBurst;
        }

        public Vector3f getOffset() {
            return offset;
        }

        public Vector4f getColor() {
            return color;
        }

        public Builder setColor(Vector4f color) {
            this.color = color;
            return this;
        }

        public Vector3f getVelocity() {
            return velocity;
        }

        public Builder setVelocity(Vector3f velocity) {
            this.velocity = velocity;
            return this;
        }

        public Vector2f getSize() {
            return size;
        }

        public Builder setSize(Vector2f size) {
            this.size = size;
            return this;
        }

        void writeToBuffer(PacketWriteBuffer buffer) throws IOException {
            buffer.writeInt(amount);
            buffer.writeInt(lifetime);
            buffer.writeInt(randomLife);
            buffer.writeFloat(speed);
            buffer.writeBoolean(emissionBurst);
            buffer.writeVector3f(offset);
            buffer.writeVector3f(velocity);
            buffer.writeVector4f(color);
            buffer.writeVector2f(size);
        }

        void readFromBuffer(PacketReadBuffer buffer) throws IOException {
            amount = buffer.readInt();
            lifetime = buffer.readInt();
            randomLife = buffer.readInt();
            speed = buffer.readFloat();
            emissionBurst = buffer.readBoolean();
            offset = buffer.readVector3f();
            velocity = buffer.readVector3f();
            color = buffer.readVector4f();
            size = buffer.readVector2f();
        }
    }
}
