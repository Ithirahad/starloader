package api.utils.gui;

import api.DebugFile;
import api.mod.ModSkeleton;
import api.utils.other.HashList;
import java.util.ArrayList;

/**
 * ControlManagerHandler.java
 * Registers and handles custom mod control managers.
 *
 * @since 3/17/2021
 * @author TheDerpGamer
 */
public class ControlManagerHandler {

    private static HashList<ModSkeleton, GUIControlManager> modControlManagers = new HashList<>();

    /**
     * Registers a new control manager.
     * @see api.listener.events.gui.ControlManagerActivateEvent to test if the control manager is activated.
     * @param mod The mod calling this method.
     * @param controlManager The control manager (should extend AbstractControlManager).
     */
    public static void registerNewControlManager(ModSkeleton mod, GUIControlManager controlManager) {
        modControlManagers.add(mod, controlManager);
        DebugFile.log("Registered new control manager " + controlManager.getClass().getName() + " from mod " + mod.getName() + ".");
    }

    public static void deactivateAll() {
        for(GUIControlManager controlManager : getAllModControlManagers()) controlManager.setActive(false);
    }

    public static ArrayList<GUIControlManager> getModControlManagers(ModSkeleton modSkeleton) {
        return modControlManagers.getList(modSkeleton);
    }

    public static ArrayList<GUIControlManager> getAllModControlManagers() {
        ArrayList<GUIControlManager> controlManagers = new ArrayList<>();
        for(ModSkeleton modSkeleton : modControlManagers.keySet()) {
            controlManagers.addAll(getModControlManagers(modSkeleton));
        }
        return controlManagers;
    }
}
