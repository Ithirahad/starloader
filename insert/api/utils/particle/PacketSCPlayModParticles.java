package api.utils.particle;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import java.io.IOException;

/**
 * Created by Jake on 12/8/2020.
 * Not implemented
 */

public class PacketSCPlayModParticles extends Packet {
    private int factoryId;
    int sprite;
    Vector3f worldPos;
    IModParticleFactory factory;
    private ModParticleUtil.Builder builder;

    public PacketSCPlayModParticles(int factoryId, int sprite, Vector3f worldPos, ModParticleUtil.Builder builder) {
        this.factoryId = factoryId;
        this.sprite = sprite;
        this.worldPos = worldPos;
        this.factory = ModParticleVertexBuffer.getFactories().get(factoryId);
        this.builder = builder;
    }

    public PacketSCPlayModParticles() {
    }

    @Override
    public void readPacketData(PacketReadBuffer buf) throws IOException {
        sprite = buf.readInt();
        worldPos = buf.readVector3f();
        factoryId = buf.readInt();

        factory = ModParticleVertexBuffer.getFactories().get(factoryId);
        builder = new ModParticleUtil.Builder();
        builder.readFromBuffer(buf);
    }

    @Override
    public void writePacketData(PacketWriteBuffer buf) throws IOException {
        buf.writeInt(sprite);
        buf.writeVector3f(worldPos);
        buf.writeInt(factoryId);
        builder.writeToBuffer(buf);
    }

    @Override
    public void processPacketOnClient() {
        ModParticleUtil.playClient(factoryId, worldPos, sprite, builder);
    }

    @Override
    public void processPacketOnServer(PlayerState sender) {
    }
}
