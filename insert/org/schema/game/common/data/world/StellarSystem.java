//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.common.data.world;

import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SpaceStation.SpaceStationType;
import org.schema.game.common.data.world.SectorInformation.PlanetType;
import org.schema.game.common.data.world.SectorInformation.SectorType;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GalaxyTmpVars;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.simulation.npc.geo.FactionResourceRequestContainer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.Tag.Type;
import org.schema.schine.resource.tag.TagSerializable;

import javax.vecmath.Vector3f;
import java.util.Random;

public abstract class StellarSystem implements TagSerializable {
    public static final int ORBIT_COUNT_MAX = 8;
    public static final int DATA_SIZE = 2;
    private static final float starSystemSize = 6.5F;
    public static boolean debug;
    private final Vector3i position = new Vector3i();
    private final int[] orbitTakenByGeneration = new int[8];
    public int[] orbitCircularWaitAmount = new int[8];
    public double lastCheckOwner;
    protected byte[] infos;
    private long simulationStart;
    private SectorType centerSectorType;
    private long DBId = -1L;
    private long universeDay;
    private Vector3i ownerPos = new Vector3i();
    private int ownerFaction;
    private String ownerUID;
    public FactionResourceRequestContainer systemResources = new FactionResourceRequestContainer();

    public StellarSystem(byte[] var1) {
        this.infos = var1;
    }

    public StellarSystem() {
    }

    public static Vector3i getPosFromSector(Vector3i var0, Vector3i var1) {
        return VoidSystem.getContainingSystem(var0, var1);
    }

    public static boolean isBorderSystem(Vector3i var0) {
        int var1 = ByteUtil.modU16(var0.x);
        int var2 = ByteUtil.modU16(var0.y);
        int var4 = ByteUtil.modU16(var0.z);
        Vector3f var3;
        (var3 = new Vector3f()).set((float)(var1 - 8), (float)(var2 - 8), (float)(var4 - 8));
        return var3.length() >= 6.5F && var3.length() < 9.0F;
    }

    public static boolean isCenter(Vector3i var0) {
        int var1 = VoidSystem.localCoordinate(var0.x);
        int var2 = VoidSystem.localCoordinate(var0.y);
        int var3 = VoidSystem.localCoordinate(var0.z);
        return var1 == 8 && var2 == 8 && var3 == 8;
    }

    public static boolean isCenterNeighbor(Vector3i var0) {
        int var1 = VoidSystem.localCoordinate(var0.x) - 8;
        int var2 = VoidSystem.localCoordinate(var0.y) - 8;
        int var3 = VoidSystem.localCoordinate(var0.z) - 8;
        return Vector3fTools.length((float)var1, (float)var2, (float)var3) < 1.42F;
    }

    public static Vector3i getLocalCoordinates(Vector3i var0, Vector3i var1) {
        int var2 = VoidSystem.localCoordinate(var0.x);
        int var3 = VoidSystem.localCoordinate(var0.y);
        int var4 = VoidSystem.localCoordinate(var0.z);
        var1.set(var2, var3, var4);
        return var1;
    }

    public static boolean isStarSystem(Vector3i var0) {
        return isStarSystem(var0.x, var0.y, var0.z);
    }

    public static boolean isStarSystem(int var0, int var1, int var2) {
        var0 = ByteUtil.modU16(var0);
        var1 = ByteUtil.modU16(var1);
        var2 = ByteUtil.modU16(var2);
        Vector3f var3;
        (var3 = new Vector3f()).set((float)(var0 - 8), (float)(var1 - 8), (float)(var2 - 8));
        return var3.length() < 6.5F;
    }

    public float getDistanceIntensity(float var1, float var2) {
        return var2 / Math.max(1.0F, var1);
    }

    public boolean isHeatDamage(Vector3i var1, float var2, float var3, float var4) {
        boolean var5 = false;
        if (this.getCenterSectorType() == SectorType.SUN || this.getCenterSectorType() == SectorType.DOUBLE_STAR || this.getCenterSectorType() == SectorType.GIANT) {
            var3 = this.getDistanceIntensity(var2, var3);
            if (ServerConfig.STAR_DAMAGE.isOn() && var2 > 0.0F && var3 < var4) {
                var5 = true;
            }
        }

        return var5;
    }

    protected abstract byte[] createInfoArray();

    public void fromTagStructure(Tag var1) {
        Tag[] var4 = (Tag[])var1.getValue();
        this.getPos().set((Vector3i)var4[0].getValue());
        this.infos = (byte[])var4[1].getValue();
        if (var4[2].getType() == Type.LONG) {
            long var2 = (Long)var4[2].getValue();
            this.setSimulationStart(Math.max(0L, this.getSimulationStart() - this.universeDay) + var2);
        }

        System.err.println("Loaded starsystem from disk: " + this.position);
    }

    public Tag toTagStructure() {
        Tag var1 = new Tag(Type.VECTOR3i, "pos", this.position);
        Tag var2 = new Tag(Type.BYTE_ARRAY, (String)null, this.infos);
        new Tag(Type.LONG, "simStartMod", this.getSimulationStart() % this.universeDay);
        return new Tag(Type.STRUCT, "StarSystem", new Tag[]{var1, var2, FinishTag.INST});
    }

    public void generate(Random var1, Galaxy var2, GameServerState var3, SectorGenerationInterface var4) {
        this.infos = this.createInfoArray();
        var2.getSystemResources(this.position, this.systemResources, new GalaxyTmpVars());
        this.generate(var1, this.infos, var2, var3, var4);
    }

    protected abstract void generate(Random var1, byte[] var2, Galaxy var3, GameServerState var4, SectorGenerationInterface var5);

    public Vector3i getAbsoluteSectorPos(int var1, Vector3i var2) {
        this.getLocalSectorPos(var1, var2);
        var2.add(this.getPos().x << 4, this.getPos().y << 4, this.getPos().z << 4);
        return var2;
    }

    public SectorType getCenterSectorType() {
        return this.centerSectorType;
    }

    public void setCenterSectorType(SectorType var1) {
        this.centerSectorType = var1;
    }

    public long getDBId() {
        return this.DBId;
    }

    public void setDBId(long var1) {
        this.DBId = var1;
    }

    public int getIndex(int var1, int var2, int var3) {
        return (var3 << 8) + (var2 << 4) + var1;
    }

    public int getIndex(Vector3i var1) {
        return this.getIndex(var1.x, var1.y, var1.z);
    }

    public byte[] getInfos() {
        return this.infos;
    }

    public abstract int getLocalCoordinate(int var1);

    public Vector3i getLocalSectorPos(int var1, Vector3i var2) {
        int var3;
        int var4 = (var3 = (var1 /= 2) / 256) << 8;
        int var5;
        int var6 = (var5 = (var1 - var4) / 16) << 4;
        var1 = (var1 - var4 - var6) % 16;
        var2.set(var1, var5, var3);
        return var2;
    }

    public String getName() {
        return "default";
    }

    public PlanetType getPlanetType(int var1) {
        var1 <<= 1;
        return PlanetType.values()[Math.min(PlanetType.values().length - 1, this.infos[var1 + 1])];
    }

    public PlanetType getPlanetType(Vector3i var1) {
        int var2 = this.getLocalCoordinate(var1.x);
        int var3 = this.getLocalCoordinate(var1.y);
        int var4 = this.getLocalCoordinate(var1.z);
        return this.getPlanetType(this.getIndex(var2, var3, var4));
    }

    public PlanetType getPlanetTypeFromAbsolute(Vector3i var1, byte[] var2) {
        int var5 = this.getLocalCoordinate(var1.x);
        int var3 = this.getLocalCoordinate(var1.y);
        int var4 = this.getLocalCoordinate(var1.z);
        var4 = this.getIndex(var5, var3, var4);
        return this.getPlanetType(var4);
    }

    public Vector3i getPos() {
        return this.position;
    }

    public SectorType getSectorType(int var1) {
        var1 <<= 1;
        return SectorType.values()[this.infos[var1]];
    }

    public SectorType getSectorType(Vector3i var1) {
        int var2 = this.getLocalCoordinate(var1.x);
        int var3 = this.getLocalCoordinate(var1.y);
        int var4 = this.getLocalCoordinate(var1.z);
        return this.getSectorType(this.getIndex(var2, var3, var4));
    }

    public SectorType getSectorTypeFromAbsolute(Vector3i var1, byte[] var2) {
        int var5 = this.getLocalCoordinate(var1.x);
        int var3 = this.getLocalCoordinate(var1.y);
        int var4 = (this.getLocalCoordinate(var1.z) << 8) + (var3 << 4) + var5;
        return this.getSectorType(var4);
    }

    public long getSimulationStart() {
        return this.simulationStart;
    }

    public void setSimulationStart(long var1) {
        this.simulationStart = var1;
    }

    public SpaceStationType getSpaceStationTypeType(int var1) {
        var1 <<= 1;
        return SpaceStationType.values()[Math.min(SpaceStationType.values().length - 1, this.infos[var1 + 1])];
    }

    public SpaceStationType getSpaceStationTypeType(Vector3i var1) {
        int var2 = this.getLocalCoordinate(var1.x);
        int var3 = this.getLocalCoordinate(var1.y);
        int var4 = this.getLocalCoordinate(var1.z);
        return this.getSpaceStationTypeType(this.getIndex(var2, var3, var4));
    }

    public float getTemperature(Vector3i var1) {
        int var2 = VoidSystem.localCoordinate(var1.x) - 8;
        int var3 = VoidSystem.localCoordinate(var1.y) - 8;
        int var5 = VoidSystem.localCoordinate(var1.z) - 8;
        Vector3f var4 = new Vector3f(7.0F, 7.0F, 7.0F);
        Vector3f var6 = new Vector3f((float)var2, (float)var3, (float)var5);
        float var7 = Math.min(1.0F, var6.length() / var4.length());
        return 1.0F - var7;
    }

    public boolean isChanged() {
        return false;
    }

    public abstract void loadInfos(Galaxy var1, byte[] var2);

    public void setPlanetType(int var1, PlanetType var2) {
        var1 <<= 1;
        this.infos[var1 + 1] = (byte)var2.ordinal();
    }

    public void setSectorType(int var1, SectorType var2) {
        var1 <<= 1;

        assert this.infos[var1] != (byte)SectorType.BLACK_HOLE.ordinal();

        this.infos[var1] = (byte)var2.ordinal();
    }

    public void setStationType(int var1, SpaceStationType var2) {
        var1 <<= 1;
        this.infos[var1 + 1] = (byte)var2.ordinal();
    }

    public String getOwnerUID() {
        return this.ownerUID;
    }

    public void setOwnerUID(String var1) {
        this.ownerUID = var1;
    }

    public int getOwnerFaction() {
        return this.ownerFaction;
    }

    public void setOwnerFaction(int var1) {
        this.ownerFaction = var1;
    }

    public Vector3i getOwnerPos() {
        return this.ownerPos;
    }

    public void setOwnerPos(Vector3i var1) {
        this.ownerPos = var1;
    }

    public int getOrbitWait(int var1) {
        return this.orbitCircularWaitAmount[var1];
    }

    public int getOrbitTakenIteration(int var1) {
        return this.orbitTakenByGeneration[var1];
    }

    public boolean isOrbitTakenByGeneration(int var1) {
        return this.orbitTakenByGeneration[var1] < 0;
    }

    public void setOrbitTakenByGeneration(int var1) {
        this.orbitTakenByGeneration[var1] = -1;
    }

    public void incrementOrbit(int var1) {
        int var10002 = this.orbitTakenByGeneration[var1]++;
    }
}
