package org.schema.game.client.controller.manager.ingame;

import api.utils.gui.ControlManagerHandler;
import api.utils.gui.GUIControlManager;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.FastMath;
import org.schema.game.client.controller.PlayerMessageLogPlayerInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.AiConfigurationManager;
import org.schema.game.client.controller.manager.ingame.catalog.CatalogControlManager;
import org.schema.game.client.controller.manager.ingame.cubatom.CubatomControllerManager;
import org.schema.game.client.controller.manager.ingame.faction.FactionControlManager;
import org.schema.game.client.controller.manager.ingame.map.MapControllerManager;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationControllerManager;
import org.schema.game.client.controller.manager.ingame.ship.FleetControlManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.ThrustManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.controller.manager.ingame.structurecontrol.StructureControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.reactor.ReactorTreeDialog;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.rails.RailController;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class PlayerGameControlManager extends AbstractControlManager {

    private final ObjectArrayList<AbstractControlManager> panelElements = new ObjectArrayList<AbstractControlManager>();
    short lastUpdateInventory = -1;
    long lastUpdateTime = 0;
    private PlayerInteractionControlManager playerIntercationManager;
    private InventoryControllerManager inventoryControlManager;
    private ShopControllerManager shopControlManager;
    private WeaponAssignControllerManager weaponControlManager;
    private FleetControlManager fleetControlManager;
    private NavigationControllerManager navigationControlManager;
    private MapControllerManager mapControlManager;
    private AiConfigurationManager aiConfigurationManager;
    private FactionControlManager factionControlManager;
    private CatalogControlManager catalogControlManager;
    private CubatomControllerManager cubatomControlManager;
    private StructureControllerManager structureControlManager;
    private ThrustManager thrustManager;

    public PlayerGameControlManager(GameClientState state) {
        super(state);
        initialize();
    }
    public void activateThrustManager(Ship ship) {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        if (inventoryControlManager.isActive()) {
            inventoryControlManager.setActive(false);
        }
        if (shopControlManager.isActive()) {
            shopControlManager.setActive(false);
        }
        if (getFactionControlManager().isActive()) {
            getFactionControlManager().setActive(false);
        }
        if (getCatalogControlManager().isActive()) {
            getCatalogControlManager().setActive(false);
        }
        if (weaponControlManager.isActive()) {
            weaponControlManager.setActive(false);
        }
        if (fleetControlManager.isActive()) {
            fleetControlManager.setActive(false);
        }
        if (mapControlManager.isActive()) {
            mapControlManager.setActive(false);
        }
        if (aiConfigurationManager.isActive()) {
            aiConfigurationManager.setActive(false);
        }
        if (cubatomControlManager.isActive()) {
            cubatomControlManager.setActive(false);
        }
        if (structureControlManager.isActive()) {
            structureControlManager.setActive(false);
        }
        if (navigationControlManager.isActive()) {
            navigationControlManager.setActive(false);
        }

        boolean act = thrustManager.isActive();

        thrustManager.setSelectedShip(ship);
        thrustManager.setActive(!act);

        if (!thrustManager.isActive()) {
            for (int i = 0; i < getState().getController().getPlayerInputs().size(); i++) {
                DialogInterface p = getState().getController().getPlayerInputs().get(i);
                if (p instanceof PlayerMessageLogPlayerInput) {
                    getState().getController().getPlayerInputs().get(i).deactivate();
                    break;
                }
            }
        }

    }
    public void aiConfigurationAction(SegmentPiece piece) {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        if (getState().getShip() != null && getState().getShip().getAiConfiguration().getControllerBlock() != null) {
            aiConfigurationManager.setCanEdit(true);
            aiConfigurationManager.setAi(getState().getShip());
        } else if (piece != null && piece.getSegment().getSegmentController() instanceof AiInterface) {
            aiConfigurationManager.setCanEdit(true);
            aiConfigurationManager.setAi((AiInterface) piece.getSegment().getSegmentController());
        } else if (getPlayerIntercationManager().getSelectedEntity() != null && getPlayerIntercationManager().getSelectedEntity() instanceof AiInterface) {

            aiConfigurationManager.setCanEdit(getState().getPlayer().getNetworkObject().isAdminClient.get());
            aiConfigurationManager.setAi((AiInterface) getPlayerIntercationManager().getSelectedEntity());
        } else {
            aiConfigurationManager.setCanEdit(false);
            aiConfigurationManager.setAi(null);
        }

        if (thrustManager.isActive()) {
            thrustManager.setActive(false);
        }
        if (inventoryControlManager.isActive()) {
            inventoryControlManager.setActive(false);
        }
        if (getFactionControlManager().isActive()) {
            getFactionControlManager().setActive(false);
        }
        if (getCatalogControlManager().isActive()) {
            getCatalogControlManager().setActive(false);
        }
        if (shopControlManager.isActive()) {
            shopControlManager.setActive(false);
        }
        if (weaponControlManager.isActive()) {
            weaponControlManager.setActive(false);
        }
        if (fleetControlManager.isActive()) {
            fleetControlManager.setActive(false);
        }
        if (navigationControlManager.isActive()) {
            navigationControlManager.setActive(false);
        }
        if (cubatomControlManager.isActive()) {
            cubatomControlManager.setActive(false);
        }
        if (structureControlManager.isActive()) {
            structureControlManager.setActive(false);
        }

        boolean activate = !aiConfigurationManager.isActive();
        aiConfigurationManager.setDelayedActive(activate);
    }

    public void deactivateAll() {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        for (AbstractControlManager a : panelElements) {
            a.setActive(false);
        }

        getPlayerIntercationManager().hinderInteraction(600);
    }

    /**
     * @return the aiConfigurationManager
     */
    public AiConfigurationManager getAiConfigurationManager() {
        return aiConfigurationManager;
    }

    /**
     * @return the catalogControlManager
     */
    public CatalogControlManager getCatalogControlManager() {
        return catalogControlManager;
    }

    /**
     * @return the cubatomControlManager
     */
    public CubatomControllerManager getCubatomControlManager() {
        return cubatomControlManager;
    }

    /**
     * @param cubatomControlManager the cubatomControlManager to set
     */
    public void setCubatomControlManager(CubatomControllerManager cubatomControlManager) {
        this.cubatomControlManager = cubatomControlManager;
    }

    /**
     * @return the cubatomControlManager
     */
    public StructureControllerManager getStructureControlManager() {
        return structureControlManager;
    }

    public FactionControlManager getFactionControlManager() {
        return factionControlManager;
    }

    /**
     * @return the inventoryControlManager
     */
    public InventoryControllerManager getInventoryControlManager() {
        return inventoryControlManager;
    }

    /**
     * @return the mapControlManager
     */
    public MapControllerManager getMapControlManager() {
        return mapControlManager;
    }

    /**
     * @return the navigationControlManager
     */
    public NavigationControllerManager getNavigationControlManager() {
        return navigationControlManager;
    }

    /**
     * @return the playerIntercationManager
     */
    public PlayerInteractionControlManager getPlayerIntercationManager() {
        return playerIntercationManager;
    }

    /**
     * @return the shopControlManager
     */
    public ShopControllerManager getShopControlManager() {
        return shopControlManager;
    }

    /**
     * @return the weaponControlManager
     */
    public WeaponAssignControllerManager getWeaponControlManager() {
        return weaponControlManager;
    }

    @Override
    public void handleKeyEvent(KeyEventInterface e) {
        /*
         * update dialogs
         */
        int size = getState().getController().getPlayerInputs().size();
        if (size > 0) {
            //only the last in list is active
            getState().getController().getPlayerInputs().get(size - 1).handleKeyEvent(e);
            return;
        }
        if (KeyboardMappings.getEventKeyState(e, getState())) {

            /*
             * Update local player keys
             */
            if (getState().getController().getPlayerInputs().isEmpty() && !getState().isDebugKeyDown()) {
                if (inventoryControlManager.isActive() && KeyboardMappings.ACTIVATE
                        .isEventKey(e, getState())) {
                    inventoryAction(null);
                }
                if (KeyboardMappings.INVENTORY_PANEL
                        .isEventKey(e, getState())) {
                    inventoryAction(null);

                } else if (KeyboardMappings.SHOP_PANEL
                        .isEventKey(e, getState())) {

                    shopAction();
                } else if (KeyboardMappings.WEAPON_PANEL.isEventKey(e, getState())
                ) {
                    weaponAction();

                } else if (KeyboardMappings.FLEET_PANEL.isEventKey(e, getState())
                ) {
                    fleetAction();

                } else if (KeyboardMappings.NAVIGATION_PANEL
                        .isEventKey(e, getState())) {

                    navigationAction();
                } else if (KeyboardMappings.CATALOG_PANEL
                        .isEventKey(e, getState())) {

                    catalogAction();

                } else if (KeyboardMappings.HELP_SCREEN.isEventKey(e, getState())) {
                    EngineSettings.CONTROL_HELP.changeBooleanSetting(!EngineSettings.CONTROL_HELP.isOn());
                } else if (KeyboardMappings.STRUCTURE_PANEL.isEventKey(e, getState())) {

                    structureAction();

                } else if (KeyboardMappings.REACTOR_KEY.isEventKey(e, getState())) {

                    if(getState().getCurrentPlayerObject() != null && getState().getCurrentPlayerObject().hasAnyReactors()){
                        ReactorTreeDialog d = new ReactorTreeDialog(getState(), (ManagedSegmentController<?>) getState().getCurrentPlayerObject());
                        d.activate();
                    }

                }else if (KeyboardMappings.MAP_PANEL
                        .isEventKey(e, getState())) {

                    galaxyMapAction();

                } else if (KeyboardMappings.AI_CONFIG_PANEL
                        .isEventKey(e, getState())) {

                    aiConfigurationAction(null);
                } else if (KeyboardMappings.FACTION_MENU.isEventKey(e, getState())) {
                    factionAction();
                } else if (KeyboardMappings.CREATIVE_MODE.isEventKey(e, getState())) {
                    if (getState().getPlayer().isHasCreativeMode()) {
                        getState().getPlayer().setUseCreativeMode(!getState().getPlayer().isUseCreativeMode());
                    }
                } else if (KeyboardMappings.SWITCH_COCKPIT_SHIP_NEXT.isEventKey(e, getState()) || KeyboardMappings.SWITCH_COCKPIT_SHIP_PREVIOUS.isEventKey(e, getState())) {

                    boolean up = KeyboardMappings.SWITCH_COCKPIT_SHIP_NEXT.isEventKey(e, getState());
                    PlayerGameControlManager playerGameControlManager = getState()
                            .getGlobalGameControlManager()
                            .getIngameControlManager()
                            .getPlayerGameControlManager();

                    handleDockingAndRail(up ? 1 : -1, playerGameControlManager);
                }

            }


        }
        boolean interactionNeedsSuspension = false;
        for (AbstractControlManager a : panelElements) {
            interactionNeedsSuspension = interactionNeedsSuspension || a.isActive();
        }

        //INSERTED CODE
        for(GUIControlManager guiControlManager : ControlManagerHandler.getAllModControlManagers()) {
            if(guiControlManager.isActive()) {
                interactionNeedsSuspension = true;
                break;
            }
        }
        //

        if (playerIntercationManager.isSuspended() != interactionNeedsSuspension) {
            // make sure this comes last to determine if hiding is needed
            playerIntercationManager.suspend(interactionNeedsSuspension);
        }
        if (getState().getController().getPlayerInputs().isEmpty()) {
            super.handleKeyEvent(e);
        }

    }

    private void handleDockingAndRail(int up, PlayerGameControlManager playerGameControlManager) {
        if(getPlayerIntercationManager().getEntered() == null){
            return;
        }

        final SegmentController current = getPlayerIntercationManager().getEntered().getSegmentController();
        if(KeyboardMappings.SWITCH_COCKPIT_SHIP_HOLD_FOR_CHAIN.isDown(getState())){

            if(current.railController.isDockedAndExecuted()){
                RailController rail = current.railController.previous.getRailRController();
                if(Math.abs(up) > rail.next.size()-1){
                    return;
                }
                int dockIndex = -1;
                for(int i = 0; i < rail.next.size(); i++){
                    RailRelation r = rail.next.get(i);
                    if(r.docked.getSegmentController() == current){
                        dockIndex = i;
                    }
                }
                //switch in same chain
                dockIndex = FastMath.cyclicModulo(dockIndex + up, rail.next.size());
                if(dockIndex >= 0 && dockIndex < rail.next.size()){
                    SegmentController c = rail.next.get(dockIndex).docked.getSegmentController();
                    if(c != current && InShipControlManager.checkEnter(c)){
                        InShipControlManager.switchEntered(c);

                    }else{
                        if(c != current){
                            getState().getController().popupInfoTextMessage(Lng.str("Skipped %s as access to it was denied", c.toNiceString()), 0);
                        }
                        handleDockingAndRail(up > 0 ? up+1 : up-1, playerGameControlManager);

                    }
                }

            }
        }else{
            SegmentController c = null;
            //switch chain
            if(up > 0){
                if(current.railController.isRail()){
                    c = current.railController.next.get(0).docked.getSegmentController();

                }
            }else{
                if(current.railController.isDockedAndExecuted()){
                    c = current.railController.previous.rail.getSegmentController();
                }
            }
            if(c != null && c != current && InShipControlManager.checkEnter(c)){
                assert(getPlayerIntercationManager().getEntered().getSegmentController() == current);
                assert(getPlayerIntercationManager().getEntered().getSegmentController() != c);

                InShipControlManager.switchEntered(c);
            }
            getState().getController().popupInfoTextMessage(Lng.str("Hold '%s' to switch to other docks\non the same docking chain level", KeyboardMappings.SWITCH_COCKPIT_SHIP_HOLD_FOR_CHAIN.getKeyChar()), 0);
        }
    }


    @Override
    public void onSwitch(boolean active) {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //
        if (active) {
            cubatomControlManager.setActive(false);
            structureControlManager.setActive(false);
            navigationControlManager.setActive(false);
            shopControlManager.setActive(false);
            weaponControlManager.setActive(false);
            fleetControlManager.setActive(false);
            inventoryControlManager.setActive(false);
            mapControlManager.setActive(false);
            thrustManager.setActive(false);
            playerIntercationManager.setDelayedActive(true);
        } else {
            // close inventory
            inventoryControlManager.setActive(false);
            navigationControlManager.setActive(false);
            shopControlManager.setActive(false);
            weaponControlManager.setActive(false);
            fleetControlManager.setActive(false);
            mapControlManager.setActive(false);
            cubatomControlManager.setActive(false);
            thrustManager.setActive(false);
            structureControlManager.setActive(false);
        }
        super.onSwitch(active);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.schema.game.client.controller.manager.AbstractControlManager#update
     * (org.schema.schine.graphicsengine.core.Timer)
     */
    @Override
    public void update(Timer timer) {
        boolean interactionNeedsSuspension = inventoryControlManager.isActive()
                || shopControlManager.isActive()
                || weaponControlManager.isActive()
                || factionControlManager.isActive()
                || aiConfigurationManager.isActive()
                || fleetControlManager.isActive()
                || catalogControlManager.isActive();

        //INSERTED CODE
        for(AbstractControlManager controlManager : ControlManagerHandler.getAllModControlManagers()) {
            if(controlManager.isActive()) {
                interactionNeedsSuspension = true;
                break;
            }
        }
        //

        if (playerIntercationManager.isSuspended() != interactionNeedsSuspension) {
            // make sure this comes last to determine if hiding is needed
            playerIntercationManager.suspend(interactionNeedsSuspension);
        }

        super.update(timer);
    }

    public void galaxyMapAction() {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        if (inventoryControlManager.isActive()) {
            inventoryControlManager.setActive(false);
        }
        if (shopControlManager.isActive()) {
            shopControlManager.setActive(false);
        }
        if (getFactionControlManager().isActive()) {
            getFactionControlManager().setActive(false);
        }
        if (getCatalogControlManager().isActive()) {
            getCatalogControlManager().setActive(false);
        }
        if (weaponControlManager.isActive()) {
            weaponControlManager.setActive(false);
        }
        if (fleetControlManager.isActive()) {
            fleetControlManager.setActive(false);
        }
        if (navigationControlManager.isActive()) {
            navigationControlManager.setActive(false);
        }
        if (cubatomControlManager.isActive()) {
            cubatomControlManager.setActive(false);
        }
        if (structureControlManager.isActive()) {
            structureControlManager.setActive(false);
        }
        if (aiConfigurationManager.isActive()) {
            aiConfigurationManager.setActive(false);
        }
        if (thrustManager.isActive()) {
            thrustManager.setActive(false);
        }
        boolean activate = !mapControlManager.isActive();
        System.err.println("ACTIVATE MAP " + activate);
        mapControlManager.setDelayedActive(activate);
    }

    public void navigationAction() {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        if (inventoryControlManager.isActive()) {
            inventoryControlManager.setActive(false);
        }
        if (shopControlManager.isActive()) {
            shopControlManager.setActive(false);
        }
        if (getFactionControlManager().isActive()) {
            getFactionControlManager().setActive(false);
        }
        if (getCatalogControlManager().isActive()) {
            getCatalogControlManager().setActive(false);
        }
        if (weaponControlManager.isActive()) {
            weaponControlManager.setActive(false);
        }
        if (fleetControlManager.isActive()) {
            fleetControlManager.setActive(false);
        }
        if (mapControlManager.isActive()) {
            mapControlManager.setActive(false);
        }
        if (aiConfigurationManager.isActive()) {
            aiConfigurationManager.setActive(false);
        }
        if (cubatomControlManager.isActive()) {
            cubatomControlManager.setActive(false);
        }
        if (structureControlManager.isActive()) {
            structureControlManager.setActive(false);
        }
        if (thrustManager.isActive()) {
            thrustManager.setActive(false);
        }
        boolean activate = !navigationControlManager.isActive();
        navigationControlManager.setDelayedActive(activate);
    }

    public void initialize() {
        playerIntercationManager = new PlayerInteractionControlManager(getState());
        inventoryControlManager = new InventoryControllerManager(getState());
        shopControlManager = new ShopControllerManager(getState());
        weaponControlManager = new WeaponAssignControllerManager(getState());
        fleetControlManager = new FleetControlManager(getState());
        navigationControlManager = new NavigationControllerManager(getState());
        mapControlManager = new MapControllerManager(getState());
        aiConfigurationManager = new AiConfigurationManager(getState());
        factionControlManager = new FactionControlManager(getState());
        catalogControlManager = new CatalogControlManager(getState());
        cubatomControlManager = new CubatomControllerManager(getState());
        structureControlManager = new StructureControllerManager(getState());
        thrustManager = (new ThrustManager(getState()));

        getControlManagers().add(thrustManager);
        getControlManagers().add(playerIntercationManager);
        getControlManagers().add(inventoryControlManager);
        getControlManagers().add(shopControlManager);
        getControlManagers().add(weaponControlManager);
        getControlManagers().add(fleetControlManager);
        getControlManagers().add(navigationControlManager);
        getControlManagers().add(aiConfigurationManager);
        getControlManagers().add(mapControlManager);
        getControlManagers().add(getFactionControlManager());
        getControlManagers().add(getCatalogControlManager());
        getControlManagers().add(getCubatomControlManager());
        getControlManagers().add(structureControlManager);

        panelElements.add(thrustManager);
        panelElements.add(inventoryControlManager);
        panelElements.add(shopControlManager);
        panelElements.add(weaponControlManager);
        panelElements.add(fleetControlManager);
        panelElements.add(navigationControlManager);
        panelElements.add(aiConfigurationManager);
        panelElements.add(mapControlManager);
        panelElements.add(cubatomControlManager);
        panelElements.add(getFactionControlManager());
        panelElements.add(getCatalogControlManager());
        panelElements.add(structureControlManager);
    }
    public boolean isAnyMenuActive() {
        for(AbstractControlManager c : getControlManagers()){
            if(c.isActive() && c != playerIntercationManager){
                return true;
            }
        }
        for(AbstractControlManager controlManager : ControlManagerHandler.getAllModControlManagers()) {
            if(controlManager.isActive()) return true;
        }

        return false;
    }
    public ThrustManager getThrustManager() {
        return thrustManager;
    }
    public void inventoryAction(Inventory inventory) {
        inventoryAction(inventory, !inventoryControlManager.isActive(), false);
    }

    public void inventoryAction(Inventory inventory, boolean activate, boolean decativeBefore) {
        if (getState().getNumberOfUpdate() == lastUpdateInventory && System.currentTimeMillis() - lastUpdateTime < 1000) {
            return;
        }
        lastUpdateInventory = getState().getNumberOfUpdate();
        lastUpdateTime = System.currentTimeMillis();

        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        if (shopControlManager.isActive()) {
            shopControlManager.setActive(false);
        }
        if (getWeaponControlManager().isActive()) {
            getWeaponControlManager().setActive(false);
        }
        if (aiConfigurationManager.isActive()) {
            aiConfigurationManager.setActive(false);
        }
        if (navigationControlManager.isActive()) {
            navigationControlManager.setActive(false);
        }
        if (fleetControlManager.isActive()) {
            fleetControlManager.setActive(false);
        }
        if (cubatomControlManager.isActive()) {
            cubatomControlManager.setActive(false);
        }

        if (structureControlManager.isActive()) {
            structureControlManager.setActive(false);
        }

        if (getFactionControlManager().isActive()) {
            getFactionControlManager().setActive(false);
        }
        if (mapControlManager.isActive()) {
            mapControlManager.setActive(false);
        }
        if (getCatalogControlManager().isActive()) {
            getCatalogControlManager().setActive(false);
        }
        if (decativeBefore) {
            inventoryControlManager.setActive(false);
        }
        if (thrustManager.isActive()) {
            thrustManager.setActive(false);
        }
        if (activate) {
            inventoryControlManager.setSecondInventory(inventory);
        }
        inventoryControlManager.setActive(activate);
    }

    public void shopAction() {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        boolean activate = !shopControlManager.isActive();

        if (!getState().isInShopDistance()) {
            getState().getController().popupInfoTextMessage(
                    Lng.str("ERROR: You are not near any shop!"), 0);
        } else {
            if (inventoryControlManager.isActive()) {
                inventoryControlManager.setActive(false);
            }
            if (mapControlManager.isActive()) {
                mapControlManager.setActive(false);
            }
            if (getFactionControlManager().isActive()) {
                getFactionControlManager().setActive(false);
            }
            if (weaponControlManager.isActive()) {
                weaponControlManager.setActive(false);
            }
            if (fleetControlManager.isActive()) {
                fleetControlManager.setActive(false);
            }
            if (navigationControlManager.isActive()) {
                navigationControlManager.setActive(false);
            }
            if (cubatomControlManager.isActive()) {
                cubatomControlManager.setActive(false);
            }
            if (structureControlManager.isActive()) {
                structureControlManager.setActive(false);
            }
            if (getCatalogControlManager().isActive()) {
                getCatalogControlManager().setActive(false);
            }
            if (aiConfigurationManager.isActive()) {
                aiConfigurationManager.setActive(false);
            }
            if (thrustManager.isActive()) {
                thrustManager.setActive(false);
            }
            shopControlManager.setDelayedActive(activate);
        }
    }

    public void weaponAction() {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        if (!getPlayerIntercationManager()
                .getInShipControlManager().isActive()) {
            getState()
                    .getController()
                    .popupAlertTextMessage(
                            Lng.str("ERROR: Weapon Menu only available\ninside a ship!"),
                            0);
            return;
        }
        if (getFactionControlManager().isActive()) {
            getFactionControlManager().setActive(false);
        }
        if (getCatalogControlManager().isActive()) {
            getCatalogControlManager().setActive(false);
        }
        if (inventoryControlManager.isActive()) {
            inventoryControlManager.setActive(false);
        }
        if (shopControlManager.isActive()) {
            shopControlManager.setActive(false);
        }
        if (mapControlManager.isActive()) {
            mapControlManager.setActive(false);
        }
        if (fleetControlManager.isActive()) {
            fleetControlManager.setActive(false);
        }
        if (navigationControlManager.isActive()) {
            navigationControlManager.setActive(false);
        }
        if (cubatomControlManager.isActive()) {
            cubatomControlManager.setActive(false);
        }
        if (structureControlManager.isActive()) {
            structureControlManager.setActive(false);
        }
        if (aiConfigurationManager.isActive()) {
            aiConfigurationManager.setActive(false);
        }
        if (thrustManager.isActive()) {
            thrustManager.setActive(false);
        }
        boolean activate = !weaponControlManager.isActive();
        weaponControlManager.setDelayedActive(activate);
    }

    public void fleetAction() {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        if (inventoryControlManager.isActive()) {
            inventoryControlManager.setActive(false);
        }
        if (shopControlManager.isActive()) {
            shopControlManager.setActive(false);
        }
        if (getFactionControlManager().isActive()) {
            getFactionControlManager().setActive(false);
        }
        if (getCatalogControlManager().isActive()) {
            getCatalogControlManager().setActive(false);
        }
        if (weaponControlManager.isActive()) {
            weaponControlManager.setActive(false);
        }
        if (mapControlManager.isActive()) {
            mapControlManager.setActive(false);
        }
        if (aiConfigurationManager.isActive()) {
            aiConfigurationManager.setActive(false);
        }
        if (navigationControlManager.isActive()) {
            navigationControlManager.setActive(false);
        }
        if (cubatomControlManager.isActive()) {
            cubatomControlManager.setActive(false);
        }
        if (thrustManager.isActive()) {
            thrustManager.setActive(false);
        }
        if (structureControlManager.isActive()) {
            structureControlManager.setActive(false);
        }
        boolean activate = !fleetControlManager.isActive();
        fleetControlManager.setDelayedActive(activate);
    }
    public void structureAction() {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        if (inventoryControlManager.isActive()) {
            inventoryControlManager.setActive(false);
        }
        if (shopControlManager.isActive()) {
            shopControlManager.setActive(false);
        }
        if (getFactionControlManager().isActive()) {
            getFactionControlManager().setActive(false);
        }
        if (getCatalogControlManager().isActive()) {
            getCatalogControlManager().setActive(false);
        }
        if (weaponControlManager.isActive()) {
            weaponControlManager.setActive(false);
        }
        if (fleetControlManager.isActive()) {
            fleetControlManager.setActive(false);
        }
        if (mapControlManager.isActive()) {
            mapControlManager.setActive(false);
        }
        if (aiConfigurationManager.isActive()) {
            aiConfigurationManager.setActive(false);
        }
        if (navigationControlManager.isActive()) {
            navigationControlManager.setActive(false);
        }
        if (cubatomControlManager.isActive()) {
            cubatomControlManager.setActive(false);
        }
        if (thrustManager.isActive()) {
            thrustManager.setActive(false);
        }
        boolean activate = !structureControlManager.isActive();
        structureControlManager.setDelayedActive(activate);
    }

    public void catalogAction() {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getStructureControlManager().setActive(false);
        getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getAiConfigurationManager().setActive(false);
        getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().setActive(false);
        getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager().setActive(false);
        getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getNavigationControlManager().setActive(false);
        getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getWeaponControlManager().setActive(false);
        getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().setActive(false);
        if(fleetControlManager.isActive()){
            fleetControlManager.setActive(false);
        }
        if (thrustManager.isActive()) {
            thrustManager.setActive(false);
        }
        getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager().setActive(true);
    }

    public void factionAction() {
        //INSERTED CODE
        ControlManagerHandler.deactivateAll();
        //

        if (inventoryControlManager.isActive()) {
            inventoryControlManager.setActive(false);
        }
        if (shopControlManager.isActive()) {
            shopControlManager.setActive(false);
        }
        if (navigationControlManager.isActive()) {
            navigationControlManager.setActive(false);
        }
        if (fleetControlManager.isActive()) {
            fleetControlManager.setActive(false);
        }
        if (cubatomControlManager.isActive()) {
            cubatomControlManager.setActive(false);
        }
        if (structureControlManager.isActive()) {
            structureControlManager.setActive(false);
        }
        if (getCatalogControlManager().isActive()) {
            getCatalogControlManager().setActive(false);
        }
        if (mapControlManager.isActive()) {
            mapControlManager.setActive(false);
        }
        if (weaponControlManager.isActive()) {
            weaponControlManager.setActive(false);
        }
        if (aiConfigurationManager.isActive()) {
            aiConfigurationManager.setActive(false);
        }
        if (thrustManager.isActive()) {
            thrustManager.setActive(false);
        }
        boolean activate = !getFactionControlManager().isActive();
        getFactionControlManager().setDelayedActive(activate);
    }
    public FleetControlManager getFleetControlManager() {
        return fleetControlManager;
    }


}