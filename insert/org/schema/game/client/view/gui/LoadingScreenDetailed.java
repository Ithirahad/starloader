/*
 * ORIGINAL STARMADE CLASS
 *
 * NOT DECOMPILED
 */

package org.schema.game.client.view.gui;

import api.mod.StarLoader;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.glu.GLU;
import org.schema.common.util.StringTools;
import org.schema.common.util.data.DataUtil;
import org.schema.game.client.controller.GameMainMenuController;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.*;
import org.schema.schine.graphicsengine.forms.Light;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;

import javax.vecmath.Vector4f;
import java.awt.*;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class LoadingScreenDetailed extends LoadingScreen{
    private final GUITextOverlay loadInfo;
    private final GUITextOverlay loadMessage;
    //INSERTED CODE
    private final GUITextOverlay modMainInfo;
    private final GUITextOverlay modSecondaryInfo;
    private final GUITextOverlay starloaderVersion;
    public static String modMainStatus = "[No Mod Initialization]";
    public static String modSecondaryStatus = "...";

    //COLOR PALETTE
    enum Palette{
        BACKGROUND(new Color(0, 0, 0)),
        MILD(new Color(255, 0, 111)),
        IMPORTANT(new Color(255, 0, 0)),
        SL_INFO(new Color(255, 0, 10)),
        DEFAULT(new Color(232, 232, 232)),
        ;

        Vector4f color;
        Palette(Color awtColor){
            color = LoadingScreenDetailed.toSlickColor(awtColor);
        }
    }
    //
    private boolean init;
    private Light light;
    private float adviceStartTime;
    private float time;
    private GameMainMenuController mainMenu;
    private List<String> tips;


    private void initLoadingTips() {
        this.tips = new ArrayList(Arrays.asList(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_5, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_7, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_9, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_22, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_11, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_12, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_53, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_15, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_17, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_18, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_19, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_21, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_13, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_16, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_25, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_23, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_27, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_24, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_28, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_30, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_31, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_32, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_33, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_34, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_35, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_36, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_37, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_38, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_39, new Object[]{Keyboard.getKeyName(KeyboardMappings.RADIAL_MENU.getMapping())}), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_40, new Object[]{Keyboard.getKeyName(KeyboardMappings.BUILD_MODE_FIX_CAM.getMapping())}), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_41, new Object[]{Keyboard.getKeyName(KeyboardMappings.BUILD_MODE_FIX_CAM.getMapping())}), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_42, new Object[]{Keyboard.getKeyName(KeyboardMappings.BUILD_MODE_FIX_CAM.getMapping())}), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_43, new Object[]{Keyboard.getKeyName(KeyboardMappings.BUILD_MODE_FIX_CAM.getMapping())}), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_44, new Object[]{Keyboard.getKeyName(KeyboardMappings.BUILD_MODE_FIX_CAM.getMapping())}), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_29, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_46, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_6, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_47, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_45, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_51, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_52, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_48, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_10, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_49, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_14, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_57, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_58, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_59, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_60, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_62, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_63, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_26, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_65, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_66, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_67, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_68, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_69, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_70, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_LOADINGSCREENDETAILED_71));
    }

    public String getRandomTip() {
        int i = new Random().nextInt(tips.size());
        return tips.get(i);
    }

    public LoadingScreenDetailed(){
        initLoadingTips();
        System.err.println("[INIT] Creating Loading Screen");
        loadInfo = new GUITextOverlay(300, 300, FontLibrary.getBlenderProHeavy30(), null);
        System.err.println("[INIT] Creating Loading Message");
        loadMessage = new GUITextOverlay(300, 300, FontLibrary.getBlenderProHeavy20(), null);
        System.err.println("[INIT] Creating Light");
        light = new Light(0);
        //INSERTED CODE
        modMainInfo = new GUITextOverlay(300, 300, FontLibrary.getBlenderProMedium22(), null);
        modSecondaryInfo = new GUITextOverlay(300, 300, FontLibrary.getBlenderProHeavy20(), null);
        starloaderVersion = new GUITextOverlay(300, 300, FontLibrary.getBlenderProHeavy20(), null);
        //
    }

    @Override
    public void drawLoadingScreen() {
        if(!init){
            onInit();
        }
        draw2d();

        draw3d();

    }



    private void draw3d() {

        Mesh mesh = Controller.getResLoader().getMesh("3dLogo");

        GUIElement.enableOrthogonal3d();
        GlUtil.glPushMatrix();

        //draw on top of screen
        GlUtil.translateModelview(GLFrame.getWidth() - 170, GLFrame.getHeight() - 140, 0);

        //y axis has to be flipped (cause: orthographic projection used)
        GlUtil.scaleModelview(40f, -40f, 40f);

        GlUtil.glColor4f(0.3f, 0.3f, .3f, 1.0f);
        GL11.glColorMask(true, true, true, true);
        GlUtil.glDepthMask(true);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        GlUtil.glEnable(GL11.GL_DEPTH_TEST);
        GlUtil.glEnable(GL11.GL_NORMALIZE);
        GlUtil.glDisable(GL11.GL_LIGHTING);
        GlUtil.glDisable(GL11.GL_TEXTURE_2D);
        GlUtil.glDisable(GL12.GL_TEXTURE_3D);
        GlUtil.glDisable(GL11.GL_CULL_FACE);
        GlUtil.glDisable(GL11.GL_COLOR_MATERIAL);
        GL20.glUseProgram(0);

        FloatBuffer dif = GlUtil.getDynamicByteBuffer(4*4, 0).asFloatBuffer();
        dif.put(0.6f);dif.put(0.6f);dif.put(0.6f);dif.put(0.9f);
        ((Buffer) dif).rewind();


        FloatBuffer spec = GlUtil.getDynamicByteBuffer(4*4, 1).asFloatBuffer();
        spec.put(0.9f);spec.put(0.9f);spec.put(0.9f);spec.put(0.9f);
        ((Buffer) spec).rewind();

        IntBuffer shine = GlUtil.getDynamicByteBuffer(4*4, 2).asIntBuffer();
        shine.put(66);shine.put(66);shine.put(66);shine.put(66);
        ((Buffer) shine).rewind();



        GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_DIFFUSE, dif);
        GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_SPECULAR, spec);
        GL11.glMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_SHININESS, shine);

        //			mesh.draw();
        //		mesh.getMaterial().attach(0);
        //			GlUtil.glDisable(GL11.GL_LIGHTING);
        light.setAmbience(new Vector4f(0.38f, .38f, .38f, 1f));
        light.setDiffuse(new Vector4f(.68f, .68f, .68f, 1f));
        light.setSpecular(new Vector4f(.99f, .99f, .99f, 1f));
        light.setPos(0, 0, 100);
        //		light.attachSimple();
        light.draw();
        //		((Mesh)mesh.getChilds().get(1)).getMaterial().setAmbient(new float[]{0,0,0,0});
        //		((Mesh)mesh.getChilds().get(1)).getMaterial().setDiffuse(new float[]{0,0,0,0});
        //		((Mesh)mesh.getChilds().get(1)).getMaterial().setSpecular(new float[]{0,0,0,0});
//			Quat4f q = new Quat4f();
//			Matrix3f r = new Matrix3f();
//			r.rotY(time*0.01f);
//			Quat4fTools.set(r, q);
//			System.err.println(((Mesh)mesh.getChilds().get(1)).getRot4()+"; \n"+((Mesh)mesh.getChilds().get(1)).getTransform().basis);
        //		((Mesh)mesh.getChilds().get(0)).setQuatRot(new Vector4f(q.x, q.y, q.z, q.w));
        ((Mesh)mesh.getChilds().get(1)).setRot(0, time, 0f);
        GlUtil.glPushMatrix();
        ((Mesh)mesh.getChilds().get(1)).transform();
        ((Mesh)mesh.getChilds().get(1)).drawVBO();

        GlUtil.glPopMatrix();
        //		((Mesh)mesh.getChilds().get(1)).loadVBO(true);
        //		((Mesh)mesh.getChilds().get(1)).renderVBO();
        //		((Mesh)mesh.getChilds().get(1)).unloadVBO(true);

        //		mesh.getMaterial().detach();

        GlUtil.glPopMatrix();
        GUIElement.disableOrthogonal();

        GlUtil.glEnable(GL11.GL_LIGHTING);
        GlUtil.glDisable(GL11.GL_NORMALIZE);
        GlUtil.glEnable(GL11.GL_DEPTH_TEST);
        GlUtil.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

        //		Mesh m = Controller.getResLoader().getMesh("3dLogo");
        //
        //		GlUtil.glMatrixMode(GL11.GL_PROJECTION);
        //		GlUtil.glPushMatrix();
        //		GlUtil.glLoadIdentity();
        //
        //		float aspect = (float) GLFrame.getWidth() / (float) GLFrame.getHeight(); //1.333333333333333333333333f
        //
        //		GlUtil.gluPerspective(Controller.projectionMatrix, 60,
        //				aspect, 0.1f, 400, true);
        //
        //		GlUtil.glColor4f(0.2f, 1.0f, 0.3f, 1f);
        //
        //
        //		GlUtil.glMatrixMode(GL11.GL_MODELVIEW);
        //		GlUtil.glPushMatrix();
        //		GlUtil.glLoadIdentity();
        //
        //		GlUtil.lookAt (
        //				0.0f, 0.0f, 30.0f,
        //				-20.0f, -10f, 0.0f,
        //				0.0f, 1.0f, 0.0f);
        //
        //
        //		GlUtil.glEnable(GL11.GL_DEPTH_TEST);
        //		GlUtil.glEnable(GL11.GL_LIGHTING);
        //		light.draw();
        //
        //		GlUtil.glEnable(GL11.GL_COLOR_MATERIAL);
        //
        //		m.setRot(90, 0, 0);
        //		m.draw();
        ////		((Mesh)m.getChilds().get(1)).drawVBO();
        //
        //
        //		GlUtil.glPopMatrix();
        //		GlUtil.glMatrixMode(GL11.GL_PROJECTION);
        //		GlUtil.glPopMatrix();
        //		GlUtil.glMatrixMode(GL11.GL_MODELVIEW);
        //
        //		GlUtil.glDisable(GL11.GL_COLOR_MATERIAL);
        //		GlUtil.glDisable(GL11.GL_LIGHTING);
    }

    private void draw2d(){
        //INSERTED CODE
        GL11.glClearColor(Palette.BACKGROUND.color.x,Palette.BACKGROUND.color.y,Palette.BACKGROUND.color.z,Palette.BACKGROUND.color.w);
        ///
        // switch to projection mode
        GlUtil.glMatrixMode(GL11.GL_PROJECTION);
        // save previous matrix which contains the
        GlUtil.glPushMatrix();
        // settings for the perspective projection
        // reset matrix
        GlUtil.glLoadIdentity();
        // set a 2D orthographic projection
        GLU.gluOrtho2D(0, GLFrame.getWidth(), GLFrame.getHeight(), 0);
        // invert the y axis, down is positive
        // mover the origin from the bottom left corner
        // to the upper left corner
        GlUtil.glMatrixMode(GL11.GL_MODELVIEW);
        GlUtil.glLoadIdentity();

        GlUtil.glPushMatrix();


        Sprite logo = Controller.getResLoader().getSprite("schine");
        logo.setPos(30, GLFrame.getHeight() - logo.getHeight(), 0);
        logo.draw();


        Sprite logoSM = Controller.getResLoader().getSprite("SM_logo_white_nostar");
        logoSM.setPos(GLFrame.getWidth() - logoSM.getWidth() + 106 , GLFrame.getHeight() - logoSM.getHeight() + 68, 0);
        logoSM.draw();

        GlUtil.glPopMatrix();

        //		GLUT glut = new GLUT();
        GlUtil.glDisable(GL11.GL_LIGHTING);
        GlUtil.glBindTexture(GL11.GL_TEXTURE_2D, 0);
        GlUtil.glDisable(GL11.GL_BLEND);
        GlUtil.glDisable(GL11.GL_DEPTH_TEST);
        GlUtil.glDisable(GL11.GL_TEXTURE_2D);
        GlUtil.glEnable(GL11.GL_COLOR_MATERIAL);

        float x = 60;
        float y = 60;
        GlUtil.glColor4f(0.9f, 0.9f, 0.9f, 0.8f);
        y += 30;

        GUIElement.enableOrthogonal();

        loadInfo.setPos(10, 10, 0);
        loadMessage.setPos(100, 200, 0);
        //INSERTED CODE
        modMainInfo.setPos(60, 400, 0);
        modSecondaryInfo.setPos(85, 435, 0);
        starloaderVersion.setPos(320, GLFrame.getHeight()-50, 0);
        loadInfo.setColor(Palette.DEFAULT.color);
        loadMessage.setColor(Palette.DEFAULT.color);
        ///

        //change advice message every ~5 seconds
        if(loadMessage.getText().isEmpty() || time - adviceStartTime > 5000){
            adviceStartTime = time;
            loadMessage.setText(new ArrayList());
            loadMessage.getText().add(Lng.str("Advice:"));
            String[] t = getRandomTip().split("\n");
            for (int i = 0; i < t.length; i++) {
                loadMessage.getText().add(t[i]);
            }
        }

        loadInfo.getText().clear();
        loadInfo.getText().add(Controller.getResLoader().getLoadString());
        String s = LoadingScreen.serverMessage;
        if(s != null){
            loadInfo.getText().add(s);
        }
        //INSERTED CODE
        modMainInfo.getText().clear();
        modMainInfo.setColor(Palette.IMPORTANT.color);
        modMainInfo.setTextSimple(modMainStatus);

        modSecondaryInfo.getText().clear();
        modSecondaryInfo.setColor(Palette.MILD.color);
        modSecondaryInfo.setTextSimple(modSecondaryStatus);

        starloaderVersion.setColor(Palette.SL_INFO.color);
        starloaderVersion.setTextSimple("StarLoader v" + StarLoader.version + " " + StarLoader.versionName);

        starloaderVersion.draw();
        modSecondaryInfo.draw();
        modMainInfo.draw();
        ///

        loadInfo.draw();
        loadMessage.draw();
        GUIElement.disableOrthogonal();
        GlUtil.glDisable(GL11.GL_COLOR_MATERIAL);
        GlUtil.glBindTexture(GL11.GL_TEXTURE_2D, 0);
        GlUtil.glEnable(GL11.GL_DEPTH_TEST);
        GlUtil.glMatrixMode(GL11.GL_PROJECTION);
        GlUtil.glPopMatrix();
        GlUtil.glMatrixMode(GL11.GL_MODELVIEW);
    }
    private static Vector4f toSlickColor(java.awt.Color awt){
        return new Vector4f(awt.getRed()/255F,awt.getGreen()/255F,awt.getBlue()/255F,1F);
    }
    private void onInit() {

        loadInfo.onInit();
        loadMessage.onInit();
        init = true;

        //INSERTED CODE
        modMainInfo.onInit();
        modSecondaryInfo.onInit();
        starloaderVersion.onInit();
        //
    }

    @Override
    public void loadInitialResources() throws IOException, ResourceException {




        // load logo first
        Controller.getResLoader().getImageLoader().loadImage(
                DataUtil.dataPath
                        + "./image-resource/schine.png",
                "schine");

        Controller.getResLoader().getImageLoader().loadImage(
                DataUtil.dataPath
                        + "./image-resource/loadingscreen-background.png",
                "loadingscreen-background");
        Controller.getResLoader().getImageLoader().loadImage(
                DataUtil.dataPath
                        + "./image-resource/SM_logo_white_nostar.png",
                "SM_logo_white_nostar");

        Controller.getResLoader().loadModelDirectly(
                "3dLogo", "./models/3Dlogo/", "3Dlogo");


        GlUtil.printGlError();
        System.out.println("[GLFrame] loading content data");
        Controller.getResLoader().setLoadString(
                "preparing data");
    }

    @Override
    public void update(Timer timer) {
        float t = (Math.min(3f, timer.getDelta()) * 500f);
        time += t;

    }

    @Override
    public void handleException(Exception e) {
        if(mainMenu == null){
            GLFrame.processErrorDialogException(e, null);
        }else{
            mainMenu.errorDialog(e);
        }
    }

    public GameMainMenuController getMainMenu() {
        return mainMenu;
    }

    public void setMainMenu(GameMainMenuController mainMenu) {
        this.mainMenu = mainMenu;
    }

}
